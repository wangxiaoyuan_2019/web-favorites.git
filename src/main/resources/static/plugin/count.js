(function($){
    function toThousands(num) {
        return (num || 0).toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
    }
    $.fn.numberRock=function(options){
        var defaults={
            speed:24,
            count:100
        };
        var opts=$.extend({}, defaults, options);

        var div_by = 100,
        count=opts["count"],
        speed = Math.floor(count / div_by),
        sum=0,
        $display = this,
        run_count = 1,
        int_speed = opts["speed"];
        var int = setInterval(function () {
            if (run_count <= div_by&&speed!=0) {
                $display.text(toThousands(sum=speed * run_count));
                run_count++;
            } else if (sum < count) {
                $display.text(toThousands(++sum));
            } else {
                clearInterval(int);
            }
        }, int_speed);
        return this;
    }

})(jQuery);

