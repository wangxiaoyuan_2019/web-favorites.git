function changeNum(num) {
    var moneyUnits = ["", "万+", "亿+", "万亿+"]
    var dividend = 10000;
    var currentNum = num;
    //转换数字
    var currentUnit = moneyUnits[0];
    //转换单位
    for (var i = 0; i < 4; i++) {
        currentUnit = moneyUnits[i]
        if (strNumSize(currentNum) < 5) {
            break;
        }
        currentNum = currentNum / dividend
    }
    var m = {};
    m.num = currentNum;
    m.unit = currentUnit;
    return m;
}

function strNumSize(tempNum) {
    var stringNum = tempNum.toString()
    var index = stringNum.indexOf(".")
    var newNum = stringNum;
    if (index != -1) {
        newNum = stringNum.substring(0, index)
    }
    return newNum.length
}