// 使用组件
layui.use(['layer'], function(){
    var layer = layui.layer;

    $.ajax({
        type: "GET",
        url: "system/config",
        dataType: "json",
        headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
        success: function (result) {
        }
    });

    $("#restart").click(function(){
        layer.confirm('确认重启服务吗?', function(index){
            layer.close(index);

            layer.load();
            $.ajax({
                type: "POST",
                url: "system/restart",
                dataType: "json",
                headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
                success: function (result) {
                    layer.closeAll('loading');
                    if (result.code == 0) {
                        layer.msg("操作成功，服务将在2秒后重启...", {icon: 6});
                    } else {
                        layer.msg(result.msg, {icon: 5});
                    }
                }
            });
        });
    });

    $("#shutdown").click(function(){
        layer.confirm('确认停止服务吗?', function(index){
            layer.close(index);

            layer.load();
            $.ajax({
                type: "POST",
                url: "system/shutdown",
                dataType: "json",
                headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
                success: function (result) {
                    layer.closeAll('loading');
                    if (result.code == 0) {
                        layer.msg("操作成功，服务将在5秒后停止...", {icon: 6});
                    } else {
                        layer.msg(result.msg, {icon: 5});
                    }
                }
            });
        });
    });
});
