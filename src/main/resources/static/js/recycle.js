layui.extend({
  categorySelect: '{/}js/categorySelect' // {/}的意思即代表采用自有路径，即不跟随 base 路径
});
layui.use(['element', 'layer', 'table', 'categorySelect'], function() {
    var element = layui.element;
    var layer = layui.layer;
    var table = layui.table;
    var categorySelect = layui.categorySelect;

    // 加载分类
    window.loadCategoryList = function (id) {
        categorySelect.initSelect("#from");
        categorySelect.initSelect("#to");
    };
    loadCategoryList();

    // 新增分类
    $("#addCategoryIconMove").click(function () {
        layer.prompt({title: "添加分类", placeholder:"输入分类名称", maxlength: 100}, function (value, index, elem) {
            // 验证
            var bool = true;
            $.ajax({
                type: "GET",
                url: 'category/check/' + value,
                async: false,
                dataType: 'json',
                headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
                success: function (result) {
                    if (result.data) {
                        layer.tips('分类已存在', elem, {tips: 3});
                        bool = false;
                    }
                }
            });
            if(bool){
                layer.close(index);
                layer.load();
                $.ajax({
                    type: "POST",
                    url: "category",
                    data: JSON.stringify({"name": value}),
                    contentType: 'application/json;charset=utf-8',
                    dataType: "json",
                    headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
                    success: function (result) {
                        layer.closeAll('loading');
                        if (result.code == 0) {
                            var data = result.data;
                            $("#to").attr("data-value",data.id).attr("data-text",data.name).val(data.name);
                            categorySelect.loadSelect("#from");
                            categorySelect.loadSelect("#to");
                        } else {
                            layer.msg(result.msg, {icon: 5});
                        }
                    }
                });
            }
        });
    });

    //加载数据
    table.render({
        elem: '#moveList'
        , url: 'favorites/moveList/' //数据接口
        , headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")}
        , page: {
            layout: ['count', 'prev', 'page', 'next', 'skip']
            , prev: '上一页'
            , next: '下一页'
            , limit: 5
        }
        , request: {
            pageName: 'pageNum' //页码的参数名称，默认：page
            , limitName: 'pageSize' //每页数据量的参数名，默认：limit
        }
        , parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total,
                "data": res.data.list //解析数据列表
            };
        }
        , done: function (res, curr, count) {
            // 解决无数据table样式错乱
            if(count == 0){
                $("th[data-key='1-0-2']").css("border-right", "0");
            }
        }
        , cols: [[ //表头
            {type: 'checkbox'}
            , {
                field: 'name', title: '名称', minWidth: 200, templet: function (d) {
                    return '<span>' + escape(d.name) + '</span>';
                }
            }
            , {
                field: 'url', title: '地址', templet: function (d) {
                    return '<a class="layui-blue" href="javascript:openUrl(\'' + d.url + '\');">' + d.url + '</a>'
                }
            }
            , {title: '操作', width: 80, toolbar: '#moveOperates', fixed: 'right'}
        ]]
    });

    table.on('tool(moveList)', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;
        if (layEvent === 'del') { //删除
            $.ajax({
                type: "GET",
                url: "favorites/delete/" + data.id,
                dataType: "json",
                headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
                success: function (result) {
                    if (result.code == 0) {
                        obj.del(); //移除当前行
                        layer.msg('删除成功', {icon: 6});
                    } else {
                        layer.msg(result.msg, {icon: 5});
                    }
                }
            });
        }
    });

    $("#cancel").click(function(){
        $("#moveDiv").fadeOut(200);
    });

    $("#searchBtn").click(function(){
        var name = $("#name").val();
        var from = $("#from").attr("data-value");
        table.reload('moveList', {page: {curr: 1}, where: {"name": name, "categoryId": from}});
    });

    $("#moveBtn").click(function(){
        var moveAll = $("#moveAll").is(":checked") ? 1 : 0;
        var checkStatus = table.checkStatus('moveList');
        if (moveAll ==0 && checkStatus.data.length === 0) {
            layer.msg('请选择书签');
            return false;
        }
        $("#moveDiv").fadeIn(200);
    });

    // 移动
    $("#save").click(function () {
        var moveAll = $("#moveAll").is(":checked") ? 1 : 0;
        var checkStatus = table.checkStatus('moveList');
        if (moveAll == 0 && checkStatus.data.length === 0) {
            layer.msg('请选择书签');
            return false;
        }
        var ids = checkStatus.data.map(function(v){return v.id});
        var from = $("#from").attr("data-value");
        var to = $("#to").attr("data-value");
        $.ajax({
            type: "POST",
            url: "favorites/move/",
            data: JSON.stringify({"to": to, "from": from, "ids": ids, "moveAll": moveAll}),
            contentType: 'application/json;charset=utf-8',
            dataType: "json",
            headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
            success: function (result) {
                if (result.code == 0) {
                    layer.msg('操作成功', {icon: 6});
                    $("#moveDiv").fadeOut(200);
                    table.reload('moveList');
                } else {
                    layer.msg(result.msg, {icon: 5});
                }
            }
        });
    });

    //加载数据
    table.render({
        elem: '#favoritesList'
        , url: 'favorites/recycle/' //数据接口
        , headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")}
        , page: {
            layout: ['count', 'prev', 'page', 'next', 'skip']
            , prev: '上一页'
            , next: '下一页'
            , limit: 5
        }
        , request: {
            pageName: 'pageNum' //页码的参数名称，默认：page
            , limitName: 'pageSize' //每页数据量的参数名，默认：limit
        }
        , parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total,
                "data": res.data.list //解析数据列表
            };
        }
        , done: function (res, curr, count) {
            // 解决无数据table样式错乱
            if(count == 0){
                $("th[data-key='1-0-4']").css("border-right", "0");
            }
        }
        , cols: [[ //表头
            {type: 'numbers'}
            , {
                field: 'name', title: '名称', minWidth: 200, templet: function (d) {
                    return '<span>' + escape(d.name) + '</span>';
                }
            }
            , {
                field: 'url', title: '地址', templet: function (d) {
                    return '<a class="layui-blue" href="javascript:openUrl(\'' + d.url + '\');">' + d.url + '</a>'
                }
            }
            , {
                field: 'deleteTime', title: '删除时间'
            }
            , {title: '操作', width: 120, toolbar: '#operates', fixed: 'right'}
        ]]
    });

    //监听工具条
    table.on('tool(favoritesList)', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;
        if (layEvent === 'recover') { //恢复
            $.ajax({
                type: "GET",
                url: "favorites/recover/" + data.id,
                dataType: "json",
                headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
                success: function (result) {
                    if (result.code == 0) {
                        obj.del(); //移除当前行
                        layer.msg('操作成功', {icon: 6});
                    } else {
                        layer.msg(result.msg, {icon: 5});
                    }
                }
            });
        } else if (layEvent === 'del') { //删除
            $.ajax({
                type: "POST",
                url: "favorites/recycle/delete/" + data.id,
                dataType: "json",
                headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
                success: function (result) {
                    if (result.code == 0) {
                        obj.del(); //移除当前行
                        layer.msg('删除成功', {icon: 6});
                    } else {
                        layer.msg(result.msg, {icon: 5});
                    }
                }
            });
        }
    });


    //加载数据
    table.render({
        elem: '#shareList'
        , url: 'favorites/shareList/' //数据接口
        , headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")}
        , page: {
            layout: ['count', 'prev', 'page', 'next', 'skip']
            , prev: '上一页'
            , next: '下一页'
            , limit: 5
        }
        , request: {
            pageName: 'pageNum' //页码的参数名称，默认：page
            , limitName: 'pageSize' //每页数据量的参数名，默认：limit
        }
        , parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total,
                "data": res.data.list //解析数据列表
            };
        }
        , done: function (res, curr, count) {
            // 解决无数据table样式错乱
            if(count == 0){
                $("th[data-key='2-0-4']").css("border-right", "0");
            }
        }
        , cols: [[ //表头
            {type: 'numbers'}
            , {
                field: 'name', title: '名称', minWidth: 200, templet: function (d) {
                    return '<span>' + escape(d.name) + '</span>';
                }
            }
            , {
                field: 'url', title: '地址', templet: function (d) {
                    return '<a class="layui-blue" href="javascript:openUrl(\'' + d.url + '\');">' + d.url + '</a>'
                }
            }
            , {
                field: 'support', title: '收藏数', align: 'center', width: 100, templet: function (d) {
                    return transform(d.support);
                }
            }
            , {title: '操作', width: 90, toolbar: '#shareOperates', fixed: 'right'}
        ]]
    });

    //监听工具条
    table.on('tool(shareList)', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;
        if (layEvent === 'no-share') { //恢复
            $.ajax({
                type: "GET",
                url: "favorites/no-share/" + data.id,
                dataType: "json",
                headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
                success: function (result) {
                    if (result.code == 0) {
                        obj.del(); //移除当前行
                        layer.msg('操作成功', {icon: 6});
                    } else {
                        layer.msg(result.msg, {icon: 5});
                    }
                }
            });
        }
    });

    // 打开新窗口
    window.openUrl = function (url) {
        if(url.indexOf("https://") == 0 || url.indexOf("http://") == 0){
            newWin(url);
            // 记录访问次数
            $.ajax({
                type: "GET",
                url: "user/visit",
                headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")}
            });
        }else{
            layer.msg('此链接无效', {icon: 7});
        }
    };




    $(".tab-title li").click(function(){
        $(this).addClass("active").siblings().removeClass("active");
        $(".tab-content").children().eq($(this).index()).show().siblings().hide();
    });

    $("#cleanRecycle").click(function(){
        parent.layer.confirm('确认清空回收站吗?', function(index){
          parent.layer.close(index);

            $.ajax({
                type: "POST",
                url: "favorites/recycle/clean/",
                dataType: "json",
                headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
                success: function (result) {
                    if (result.code == 0) {
                        layer.msg('操作成功', {icon: 6});
                        table.reload('favoritesList');
                    } else {
                        layer.msg(result.msg, {icon: 5});
                    }
                }
            });
        });
    });

    $("#recoverAll").click(function(){
        parent.layer.confirm('确认恢复全部收藏吗?', function(index){
          parent.layer.close(index);

            $.ajax({
                type: "GET",
                url: "favorites/recover/all/",
                dataType: "json",
                headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
                success: function (result) {
                    if (result.code == 0) {
                        layer.msg('操作成功', {icon: 6});
                        table.reload('favoritesList');
                    } else {
                        layer.msg(result.msg, {icon: 5});
                    }
                }
            });
        });
    });

    $("#cleanShare").click(function(){
        parent.layer.confirm('确认清空分享吗?', function(index){
          parent.layer.close(index);

            $.ajax({
                type: "POST",
                url: "favorites/share/clean/",
                dataType: "json",
                headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
                success: function (result) {
                    if (result.code == 0) {
                        layer.msg('操作成功', {icon: 6});
                        table.reload('shareList');
                    } else {
                        layer.msg(result.msg, {icon: 5});
                    }
                }
            });
        });
    });
});