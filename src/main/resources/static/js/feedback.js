// 使用组件
layui.use(['layer', 'table', 'form'], function(){
  var layer = layui.layer;
  var table = layui.table;
  var form = layui.form;

  //加载数据
  table.render({
      elem: '#feedbackList'
      , url: 'feedback/list/' //数据接口
      , headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")}
      , page: {
          layout: ['count', 'prev', 'page', 'next', 'skip']
          , prev: '上一页'
          , next: '下一页'
          , limit: 10
      }
      , request: {
          pageName: 'pageNum' //页码的参数名称，默认：page
          , limitName: 'pageSize' //每页数据量的参数名，默认：limit
      }
      , parseData: function (res) { //res 即为原始返回的数据
          return {
              "code": res.code, //解析接口状态
              "msg": res.msg, //解析提示文本
              "count": res.data.total,
              "data": res.data.list, //解析数据列表
          };
      }
      , done: function (res, curr, count) {
          // 解决无数据table样式错乱
          if(count == 0){
              $("th[data-key='1-0-5']").css("border-right", "0");
          }
      }
      , cols: [[ //表头
          {
              field: 'isRead', title: '状态', width: 80, templet: function (d) {
                var html = '';
                switch(d.isRead) {
                    case 0:
                        html = '<span style="color:#FF5722">未读</span>';
                        break;
                    case 1:
                        html = '<span style="color:#5FB878">已读</span>';
                        break;
                    case 2:
                        html = '<span style="color:#1E9FFF">已回复</span>';
                        break;
                    default:
                        html = '<span style="color:#1E9FFF">未知</span>';
                }
                return html;
              }
          }
          , {field: 'account', width: 140, title: '账号'}
          , {title: '反馈内容', templet: function (d){
              return escape(d.content).substring(0, 100);
            }}
          , {field: 'createTime', width: 170, title: '时间'}
          , {title: '操作', width: 170, toolbar: '#operates', fixed: 'right'}
      ]]
  });

  $("#search").click(function(){
      var content = $("#content").val();
      var account = $("#account").val();
      var isRead = $("#isRead").val();
      table.reload('feedbackList', {page: {curr: 1}, where: {"account":account, "content":content, "isRead":isRead}});
  });

  initSearch("#account");

  table.on('tool(feedbackList)', function (obj) {
    var data = obj.data;
    var layEvent = obj.event;
    if (layEvent === 'notice') { //回复
      var width = (windowWidth > 800? 800 : windowWidth) - 40 + 'px';
      parent.layer.prompt({
        formType: 2,
        value: '请输入回复内容',
        title: '回复用户',
        area: [width, '350px'] //自定义文本域宽高
      }, function(value, index, elem){
        parent.layer.close(index);

        layer.load();
        $.ajax({
            type: "POST",
            url: "feedback/notice",
            data: JSON.stringify({"id": data.id, "content": value}),
            contentType: 'application/json;charset=utf-8',
            dataType: "json",
            headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
            success: function (result) {
                layer.closeAll('loading');
                if (result.code == 0) {
                    layer.msg('回复成功', {icon: 6});
                    obj.update({remark: value});
                } else {
                    layer.msg(result.msg, {icon: 5});
                }
            }
        });
      });
    } else if (layEvent === 'view'){// 查看
        var width = (windowWidth > 800? 800 : windowWidth) + 'px';
          parent.layer.open({
              area: width,
              type: 1,
              title: '详情',
              content: '<pre>' + escape(data.content) + '<div style="margin: 10px 0">回复：</div>' + escape(data.remark) + '</pre>'
          });
        // 已读
        $.ajax({
            type: "POST",
            url: "feedback/read/" + data.id,
            dataType: "json",
            headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
            success: function (result) {
                if (result.code == 0) {
                    obj.update({isRead: result.data});
                }
            }
        });
    } else if (layEvent === 'del'){// 删除
        layer.confirm('确认删除这条反馈吗？', function(index){
            layer.close(index);
            layer.load();
            $.ajax({
                type: "POST",
                url: "feedback/delete/" + data.id,
                dataType: "json",
                headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
                success: function (result) {
                    layer.closeAll('loading');
                    if (result.code == 0) {
                        layer.msg('删除成功', {icon: 6});
                        obj.del(); //删除当前行
                    } else {
                        layer.msg(result.msg, {icon: 5});
                    }
                }
            });
        });
    }
  });
});
