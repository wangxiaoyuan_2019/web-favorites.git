layui.config({
    version: 1,
    base: 'echarts/' //这个就是你放Echart.js的目录
}).use(['element', 'echarts'], function () {
    var element = layui.element;
    var echarts = layui.echarts;

    var chart1 = echarts.init(document.getElementById('chart1'));
    var chart2 = echarts.init(document.getElementById('chart2'));

    window.loadWebContent = function(type){
        chart2.showLoading();
        $.ajax({
            type: "GET",
            url: "statistic/contentOverview/" + type,
            dataType: "json",
            headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
            success: function (result) {
                if(result.code == 0){
                   chart2.hideLoading();
                   var optionChart2 = {
                     title: {
                       text: '内容分布'
                     },
                     tooltip: {
                       trigger: 'axis'
                     },
                     legend: {
                       data: ['收藏', '瞬间', '搜索', '日程', '备忘录', '分享', '文件']
                     },
                     grid: {
                       left: '3%',
                       right: '4%',
                       bottom: '3%',
                       containLabel: true
                     },
                     toolbox: {
                       feature: {
                         saveAsImage: {}
                       }
                     },
                     xAxis: {
                       type: 'category',
                       boundaryGap: false,
                       data: result.data.map(function(v){return v.createTime})
                     },
                     yAxis: {
                       type: 'value'
                     },
                     series: [
                       {
                         name: '收藏',
                         type: 'line',
                         smooth: true,
                         showSymbol: false,
                         itemStyle: {
                             normal: {
                               lineStyle: {
                                 color: '#5470c6'
                               }
                             }
                         },
                         areaStyle: {
                             normal: {
                               color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                                 {
                                   offset: 0,
                                   color: 'rgba(84, 112, 198, 0.2)',
                                 },
                                 {
                                   offset: 1,
                                   color: 'rgba(84, 112, 198, 0)',
                                 }
                               ])
                             }
                         },
                         data: result.data.map(function(v){return v.favorites})
                       },
                       {
                         name: '瞬间',
                         type: 'line',
                         smooth: true,
                         showSymbol: false,
                         data: result.data.map(function(v){return v.moments})
                       },
                       {
                         name: '搜索',
                         type: 'line',
                         smooth: true,
                         showSymbol: false,
                         data: result.data.map(function(v){return v.searchTypes})
                       },
                       {
                         name: '日程',
                         type: 'line',
                         smooth: true,
                         showSymbol: false,
                         data: result.data.map(function(v){return v.tasks})
                       },
                       {
                         name: '备忘录',
                         type: 'line',
                         smooth: true,
                         showSymbol: false,
                         data: result.data.map(function(v){return v.memorandums})
                       },
                       {
                         name: '分享',
                         type: 'line',
                         smooth: true,
                         showSymbol: false,
                         data: result.data.map(function(v){return v.shares})
                       },
                       {
                         name: '文件',
                         type: 'line',
                         smooth: true,
                         showSymbol: false,
                         data: result.data.map(function(v){return v.files})
                       }
                     ]
                   };
                   chart2.setOption(optionChart2);
                }
            }
        });
    };
    loadWebContent(1);

    window.loadContentData = function(){
        chart1.showLoading();
        $.ajax({
            type: "GET",
            url: "statistic/contentData",
            dataType: "json",
            headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
            success: function (result) {
                if(result.code == 0){
                   var data = result.data;
                   chart1.hideLoading();
                   var optionChart1 = {
                     tooltip: {
                       trigger: 'item'
                     },
                     legend: {
                       top: '5%',
                       left: 'center'
                     },
                     series: [
                       {
                         name: '内容统计',
                         type: 'pie',
                         radius: ['40%', '70%'],
                         avoidLabelOverlap: false,
                         itemStyle: {
                           borderRadius: 10,
                           borderColor: '#fff',
                           borderWidth: 2
                         },
                         label: {
                           show: false,
                           position: 'center'
                         },
                         emphasis: {
                           label: {
                             show: true,
                             fontSize: 40,
                             fontWeight: 'bold'
                           }
                         },
                         labelLine: {
                           show: false
                         },
                         data: [
                           { value: data.favorites, name: '收藏' },
                           { value: data.moments, name: '瞬间' },
                           { value: data.searchTypes, name: '搜索' },
                           { value: data.tasks, name: '日程' },
                           { value: data.memorandums, name: '备忘录' },
                           { value: data.shares, name: '分享' },
                           { value: data.files, name: '文件' }
                         ]
                       }
                     ]
                   };
                   chart1.setOption(optionChart1);
                }
            }
        });
    };
    loadContentData();
    
    window.renderNumber = function(id,num){
        var obj = changeNum(num);
        $(id).numberRock({speed:5,count:obj.num}).after(obj.unit);
    };
    
    window.loadBaseData = function(){
        $.ajax({
            type: "GET",
            url: "statistic/baseData",
            dataType: "json",
            headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
            success: function (result) {
                if(result.code == 0){
                   var data = result.data;
                   renderNumber("#admins",data.admins);
                   renderNumber("#users",data.users);
                   renderNumber("#blacks",data.blacks);
                   renderNumber("#feedbacks",data.feedbacks);
                   renderNumber("#visits",data.visits);
                   renderNumber("#searches",data.searches);
                }
            }
        });
    };
    loadBaseData();

    window.loadVisitTop10 = function(){
        $.ajax({
            type: "GET",
            url: "statistic/visitTop10",
            dataType: "json",
            headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
            success: function (result) {
                if(result.code == 0){
                   var html = '';
                   $.each(result.data, function (i, item){
                       html += '<li class="progress-item">';
                       html += '    <p class="layui-text">';
                       html += '        <span><i class="layui-icon layui-icon-username"></i><em>'+ escape(item.username) +'('+ escape(item.nickName) +')</em></span>';
                       html += '        <span>'+ item.clickCount +'</span>';
                       html += '    </p>';
                       html += '    <div class="layui-progress">';
                       html += '        <div class="layui-progress-bar layui-bg-green" lay-percent="'+ item.clickCountPercent +'%"></div>';
                       html += '    </div>';
                       html += '</li>';
                   });
                   $("#visitTop10").empty().append(html);
                   element.render('progress');
                }
            }
        });
    };
    loadVisitTop10();
    
    window.loadOnlineTop10 = function(){
        $.ajax({
            type: "GET",
            url: "statistic/onlineTop10",
            dataType: "json",
            headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
            success: function (result) {
                if(result.code == 0){
                   var html = '';
                   $.each(result.data, function (i, item){
                       html += '<li class="progress-item">';
                       html += '    <p class="layui-text">';
                       html += '        <span><i class="layui-icon layui-icon-username"></i><em>'+ escape(item.username) +'('+ escape(item.nickName) +')</em></span>';
                       html += '        <span>'+ item.onlineHour +'</span>';
                       html += '    </p>';
                       html += '    <div class="layui-progress">';
                       html += '        <div class="layui-progress-bar layui-bg-green" lay-percent="'+ item.onlineHourPercent +'%"></div>';
                       html += '    </div>';
                       html += '</li>';
                   });
                   $("#onlineTop10").empty().append(html);
                   element.render('progress');
                }
            }
        });
    };
    loadOnlineTop10();

    element.on('tab(webContentType)', function(data){
      loadWebContent(data.index + 1);
    });

});
