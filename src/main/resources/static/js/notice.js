// 使用组件
layui.use(['layer', 'form'], function(){
  var layer = layui.layer;
  var form = layui.form;

    var E = window.wangEditor;
    var editor = new E('#content');
    editor.config.height = 298;
    editor.config.showFullScreen = false;
    editor.config.placeholder = '请输入内容';
    editor.create();

    $.ajax({
        type: "GET",
        url: "notice/get",
        dataType: "json",
        headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
        success: function (result) {
            if (result.code == 0 && result.data) {
                var data = result.data;
                //给表单赋值
                form.val("noticeForm", {
                    "title": data.title
                    , "isShow": data.isShow
                    , "isNotice": data.isNotice
                });
                editor.txt.html(data.content);
            }
        }
    });

    //监听提交
    form.on('submit(formDemo)', function(data){
      var text = editor.txt.text();
      var html = editor.txt.html();
      if(!text){
        layer.msg("请输入公告内容", {icon: 5});
        return false;
      }
      data.field.content = html;
      layer.load();
      $.ajax({
          type: "POST",
          url: "notice/save",
          data: JSON.stringify(data.field),
          contentType: 'application/json;charset=utf-8',
          dataType: "json",
          headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
          success: function (result) {
              layer.closeAll('loading');
              if (result.code == 0) {
                  layer.msg("保存成功", {icon: 6});
              } else {
                  layer.msg(result.msg, {icon: 5});
              }
          }
      });
      return false;
    });

    $("#delete").click(function(){
          $("form")[0].reset();
          editor.txt.html('');
          $.ajax({
            type: "POST",
            url: "notice/delete",
            dataType: "json",
            headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
            success: function (result) {
                if (result.code == 0) {
                    layer.msg("删除成功", {icon: 6});
                } else {
                    layer.msg(result.msg, {icon: 5});
                }
            }
          });
    });
});
