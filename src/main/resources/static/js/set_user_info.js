layui.use('form', function(){
  var form = layui.form;

  //监听提交
  form.on('submit(formDemo)', function(data){
    $.ajax({
        type: "POST",
        url: "user/save",
        data: JSON.stringify(data.field),
        contentType: 'application/json;charset=utf-8',
        dataType: "json",
        headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
        success: function (result) {
            if (result.code == 0) {
                window.location.href = "index.html";
            } else {
                layer.msg(result.msg, {icon: 5});
            }
        }
    });
    return false;
  });

  form.verify({
      nickname: function (value, item) {
          var msg;
          $.ajax({
              type: "GET",
              url: 'user/nickname/check/' + value,
              async: false,
              dataType: 'json',
              headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
              success: function (result) {
                  if (result.data) {
                      msg = "昵称已被使用";
                  }
              }
          });
          return msg;
      }
  });

  // 回到首页
  $(document).on("keydown", function(event){
      if(event.ctrlKey && event.key === "h"){
          window.location.href = "index.html";
          // 阻止默认浏览器动作(W3C)
          var e = event;
          if ( e && e.preventDefault )
              e.preventDefault();
          // IE中阻止函数器默认动作的方式
          else
              window.event.returnValue = false;
          return false;
      }
  });
});