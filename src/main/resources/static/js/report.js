layui.use(['element'], function() {
   var element = layui.element;
   
   window.renderNumber = function(id,num){
        var obj = changeNum(num);
        $(id).numberRock({speed:5,count:obj.num}).after(obj.unit);
   };

   $.ajax({
        type: "GET",
        url: "user/data",
        dataType: "json",
        headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
        success: function (result) {
            if (result.code == 0) {
                var data = result.data;
                renderNumber("#clickCount",data.clickCount);
                renderNumber("#searchCount",data.searchCount);
                renderNumber("#registerDay",data.registerDay);
                renderNumber("#onlineHour",data.onlineHour);
                renderNumber("#categoryCount",data.categoryCount);
                renderNumber("#favoriteCount",data.favoriteCount);
                renderNumber("#momentCount",data.momentCount);
                renderNumber("#taskCount",data.taskCount);
                renderNumber("#navigationCount",data.navigationCount);
                renderNumber("#memorandumCount",data.memorandumCount);
                renderNumber("#searchTypeCount",data.searchTypeCount);
                renderNumber("#fileCount",data.fileCount);
                renderNumber("#shareCount",data.shareCount);
                renderNumber("#recycleCount",data.recycleCount);
                renderNumber("#clockCount",data.clockCount);
            }
        }
    });
});