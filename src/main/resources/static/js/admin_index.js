layui.use(['layer','element'], function() {
    var layer = layui.layer;
    var element = layui.element;

    window.loadUserInfo = function(){
        $.ajax({
            type: "GET",
            url: "admin/info",
            dataType: "json",
            headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
            success: function (result) {
                if(result.code == 0){
                    var permissions = result.data.permissions;
                    $(".layui-nav a[data-permission]").each(function(){
                        var permission = $(this).attr("data-permission");
                        if(permissions.contains(permission)){
                            $(this).removeClass("layui-hide");
                        }
                    });
                    $("#username").text(result.data.nickName);
                }
            }
        });
    };
    loadUserInfo();

    element.on('nav(test)', function(elem){
      var href = $(elem).attr("data-href");
      var href1 = $("#iframe").attr("src");
      if(href && href != href1){
          $("#iframe").attr("src",href);
      }
    });

    // 登出
    $("#logout").click(function () {
        layer.confirm('确认退出系统吗？', function(index){
            layer.close(index);
             $.ajax({
                  type: "GET",
                  url: "admin/logout",
                  dataType: "json",
                  headers:{"Authorization": "Bearer "+ localStorage.getItem("login_user_token")},
                  success: function (result) {
                    localStorage.clear();
                    window.location.href = "login.html";
                  }
             });
        });
    });
});