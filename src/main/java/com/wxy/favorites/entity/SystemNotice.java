package com.wxy.favorites.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "t_system_notice")
@Accessors(chain = true)
@NoArgsConstructor
@DynamicUpdate
@DynamicInsert
public class SystemNotice {

    @Id
    @Column(name = "id", columnDefinition = "int(10) comment '主键ID(自增)'")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "content", columnDefinition = "text comment '内容'")
    private String content;

    @Column(name = "title", columnDefinition = "varchar(64) comment '标题'")
    private String title;

    @Column(name = "is_show", columnDefinition = "int(1) default 0 comment '显示: 0-否 1-是'")
    private Integer isShow;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(name = "create_time", columnDefinition = "datetime comment '创建时间'")
    private Date createTime;

    @Column(name = "is_notice", columnDefinition = "int(1) default 0 comment '通知全员: 0-否 1-是'")
    private Integer isNotice;
}
