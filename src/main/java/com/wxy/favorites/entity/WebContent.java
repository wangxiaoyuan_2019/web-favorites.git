package com.wxy.favorites.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author e-Xiaoyuan.Wang
 * @since 2022/12/27 20:50
 */
@Data
@Entity
@Table(name = "t_web_content")
@Accessors(chain = true)
@NoArgsConstructor
@DynamicUpdate
@DynamicInsert
public class WebContent {

    @Id
    @Column(name = "id", columnDefinition = "int(10) comment '主键ID(自增)'")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "favorites", columnDefinition = "int(10) default 0 comment '收藏'")
    private Integer favorites;

    @Column(name = "moments", columnDefinition = "int(10) default 0 comment '瞬间'")
    private Integer moments;

    @Column(name = "search_types", columnDefinition = "int(10) default 0 comment '搜索'")
    private Integer searchTypes;

    @Column(name = "tasks", columnDefinition = "int(10) default 0 comment '日程'")
    private Integer tasks;

    @Column(name = "memorandums", columnDefinition = "int(10) default 0 comment '备忘录'")
    private Integer memorandums;

    @Column(name = "shares", columnDefinition = "int(10) default 0 comment '分享'")
    private Integer shares;

    @Column(name = "files", columnDefinition = "int(10) default 0 comment '文件'")
    private Integer files;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @Column(name = "create_time", columnDefinition = "datetime comment '创建时间'")
    private Date createTime;
}
