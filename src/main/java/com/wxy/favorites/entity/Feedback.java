package com.wxy.favorites.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "t_feedback", indexes = {@Index(columnList = "account")})
@Accessors(chain = true)
@NoArgsConstructor
@DynamicUpdate
@DynamicInsert
public class Feedback {

    @Id
    @Column(name = "id", columnDefinition = "int(10) comment '主键ID(自增)'")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "content", columnDefinition = "text comment '内容'")
    private String content;

    @Column(name = "remark", columnDefinition = "text comment '备注'")
    private String remark;

    @Column(name = "account", columnDefinition = "varchar(100) comment '用户账号'")
    private String account;

    @Column(name = "is_read", columnDefinition = "int(1) default 0 comment '已读: 0-否 1-是 2-已回复'")
    private Integer isRead;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(name = "create_time", columnDefinition = "datetime comment '创建时间'")
    private Date createTime;
}
