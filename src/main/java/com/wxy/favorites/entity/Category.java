package com.wxy.favorites.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "t_category", indexes = {@Index(columnList = "user_id")})
@Accessors(chain = true)
@NoArgsConstructor
@DynamicUpdate
@DynamicInsert
public class Category {

    @Id
    @Column(name = "id", columnDefinition = "int(10) comment '主键ID(自增)'")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "name", columnDefinition = "varchar(100) comment '名称'")
    private String name;

    @Column(name = "user_id", columnDefinition = "int(10) comment '用户ID'")
    private Integer userId;

    @Column(name = "is_system", columnDefinition = "int(1) default 0 comment '类型：1-系统分类 2-特殊分类 0-普通分类'")
    private Integer isSystem;

    @Column(name = "sort", columnDefinition = "int(4) default 0 comment '排序'")
    private Integer sort;

    @Column(name = "bookmark", columnDefinition = "int(1) default 0 comment '强制书签模式'")
    private Integer bookmark;

    @Column(name = "pinyin", columnDefinition = "varchar(600) comment '拼音'")
    private String pinyin;

    @Column(name = "pinyin_s", columnDefinition = "varchar(100) comment '拼音首字母'")
    private String pinyinS;

    @Transient
    private List<Favorites> favorites;

    @Transient
    private Integer page;

    @Transient
    private Integer count;
}
