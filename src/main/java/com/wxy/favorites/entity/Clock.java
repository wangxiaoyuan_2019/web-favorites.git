package com.wxy.favorites.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Data
@Entity
@Table(name = "t_clock", indexes = {
        @Index(columnList = "user_id"),
        @Index(columnList = "time")})
@Accessors(chain = true)
@NoArgsConstructor
@DynamicUpdate
@DynamicInsert
public class Clock {

    @Id
    @Column(name = "id", columnDefinition = "int(10) comment '主键ID(自增)'")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "content", columnDefinition = "varchar(500) comment '内容'")
    private String content;

    @Column(name = "time", columnDefinition = "varchar(64) comment '时间'")
    private String time;

    @Column(name = "type", columnDefinition = "int(1) default 0 comment '类型: 0-每天 1-周一至周五 2-自定义'")
    private Integer type;

    @Column(name = "status", columnDefinition = "int(1) default 0 comment '状态: 0-关闭 1-开启'")
    private Integer status;

    @Column(name = "cycle", columnDefinition = "varchar(64) comment '周期'")
    private String cycle;

    @Column(name = "user_id", columnDefinition = "int(10) comment '用户ID'")
    private Integer userId;
}
