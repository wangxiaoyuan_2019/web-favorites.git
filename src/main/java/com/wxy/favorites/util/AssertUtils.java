package com.wxy.favorites.util;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.wxy.favorites.core.BizException;

import java.util.Collection;
import java.util.Map;

/**
 * 断言工具类
 */
public class AssertUtils {

    public static void isTrue(boolean expression, String message) {
        Assert.isTrue(expression, () -> new BizException(message));
    }

    public static void isTrue(boolean expression, RuntimeException e) {
        Assert.isTrue(expression, () -> e);
    }

    public static void isFalse(boolean expression, String message) {
        Assert.isTrue(!expression, () -> new BizException(message));
    }

    public static void isFalse(boolean expression, RuntimeException e) {
        Assert.isTrue(!expression, () -> e);
    }

    public static void isNull(Object obj, String message) {
        Assert.isTrue(obj == null, () -> new BizException(message));
    }

    public static void isNull(Object obj, RuntimeException e) {
        Assert.isTrue(obj == null, () -> e);
    }

    public static void notNull(Object obj, String message) {
        Assert.isTrue(obj != null, () -> new BizException(message));
    }

    public static void notNull(Object obj, RuntimeException e) {
        Assert.isTrue(obj != null, () -> e);
    }

    public static void isBlank(String text, String message) {
        Assert.isTrue(StrUtil.isBlank(text), () -> new BizException(message));
    }

    public static void isBlank(String text, RuntimeException e) {
        Assert.isTrue(StrUtil.isBlank(text), () -> e);
    }

    public static void notBlank(String text, String message) {
        Assert.isTrue(!StrUtil.isBlank(text), () -> new BizException(message));
    }

    public static void notBlank(String text, RuntimeException e) {
        Assert.isTrue(!StrUtil.isBlank(text), () -> e);
    }

    public static void isEmpty(Collection<?> collection, String message) {
        Assert.isTrue(CollectionUtil.isEmpty(collection), () -> new BizException(message));
    }

    public static void isEmpty(Collection<?> collection, RuntimeException e) {
        Assert.isTrue(CollectionUtil.isEmpty(collection), () -> e);
    }

    public static void isEmpty(Map<?, ?> map, String message) {
        Assert.isTrue(CollectionUtil.isEmpty(map), () -> new BizException(message));
    }

    public static void isEmpty(Map<?, ?> map, RuntimeException e) {
        Assert.isTrue(CollectionUtil.isEmpty(map), () -> e);
    }

    public static void isEmpty(Object[] array, String message) {
        Assert.isTrue(array == null || array.length == 0, () -> new BizException(message));
    }

    public static void isEmpty(Object[] array, RuntimeException e) {
        Assert.isTrue(array == null || array.length == 0, () -> e);
    }

    public static void notEmpty(Collection<?> collection, String message) {
        Assert.isTrue(!CollectionUtil.isEmpty(collection), () -> new BizException(message));
    }

    public static void notEmpty(Collection<?> collection, RuntimeException e) {
        Assert.isTrue(!CollectionUtil.isEmpty(collection), () -> e);
    }

    public static void notEmpty(Map<?, ?> map, String message) {
        Assert.isTrue(!CollectionUtil.isEmpty(map), () -> new BizException(message));
    }

    public static void notEmpty(Map<?, ?> map, RuntimeException e) {
        Assert.isTrue(!CollectionUtil.isEmpty(map), () -> e);
    }

    public static void notEmpty(Object[] array, String message) {
        Assert.isTrue(array != null && array.length > 0, () -> new BizException(message));
    }

    public static void notEmpty(Object[] array, RuntimeException e) {
        Assert.isTrue(array != null && array.length > 0, () -> e);
    }

}
