package com.wxy.favorites.handler;

import com.wxy.favorites.constant.ErrorConstants;
import com.wxy.favorites.core.ApiResponse;
import com.wxy.favorites.core.BizException;
import com.wxy.favorites.core.ErrorId;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

@Slf4j
@RestControllerAdvice(basePackages = {"com.wxy.favorites"})
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public ApiResponse<Object> Exception(Exception e) {
        ErrorId errorId = ErrorId.get();
        log.error("系统错误：errorId = {}", errorId, e);
        return ApiResponse.fail(ErrorConstants.SERVER_ERROR_CODE, ErrorConstants.SERVER_PARAM_ERROR_MSG, errorId);
    }

    @ExceptionHandler(value = BizException.class)
    public ApiResponse<Object> BizException(BizException e) {
        return ApiResponse.fail(e.getMessage());
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    public ApiResponse<Object> AccessDeniedException(AccessDeniedException e) {
        return ApiResponse.fail(ErrorConstants.NO_PERMISSION_CODE, ErrorConstants.NO_PERMISSION_MSG);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ApiResponse<Object> MethodArgumentNotValidException(MethodArgumentNotValidException e) {
        BindingResult exceptions = e.getBindingResult();
        if (exceptions.hasErrors()) {
            List<ObjectError> errors = exceptions.getAllErrors();
            if (!errors.isEmpty()) {
                FieldError fieldError = (FieldError) errors.get(0);
                return ApiResponse.fail(fieldError.getDefaultMessage());
            }
        }
        return ApiResponse.fail(ErrorConstants.REQUEST_PARAM_ERROR_CODE, ErrorConstants.REQUEST_PARAM_ERROR_MSG);
    }
}
