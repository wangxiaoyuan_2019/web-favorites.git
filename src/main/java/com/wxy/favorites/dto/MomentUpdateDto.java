package com.wxy.favorites.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class MomentUpdateDto {

    @NotNull(message = "id不能为空")
    private Integer id;

    @NotBlank(message = "内容不能为空")
    private String content;

    @NotBlank(message = "text不能为空")
    private String text;
}
