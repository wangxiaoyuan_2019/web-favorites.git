package com.wxy.favorites.dto;

import com.wxy.favorites.entity.Category;
import com.wxy.favorites.entity.Favorites;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author e-Xiaoyuan.Wang
 * @since 2023/3/16 13:26
 */
@Data
@Accessors(chain = true)
public class SearchDto {
    private List<Favorites> favoritesList;
    private List<Category> categoryList;
}
