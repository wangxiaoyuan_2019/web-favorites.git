package com.wxy.favorites.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author e-Xiaoyuan.Wang
 * @since 2022/12/8 13:26
 */
@Data
public class SendNoticeDto {

    @NotEmpty(message = "收件人不能为空")
    private List<Integer> ids;

    @NotBlank(message = "内容不能为空")
    private String content;
}
