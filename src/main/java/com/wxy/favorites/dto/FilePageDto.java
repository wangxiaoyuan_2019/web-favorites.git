package com.wxy.favorites.dto;

import com.wxy.favorites.core.PageInfo;
import com.wxy.favorites.entity.UserFile;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author e-Xiaoyuan.Wang
 * @since 2023/3/16 9:45
 */
@Data
@Accessors(chain = true)
public class FilePageDto {
    private Integer parent;
    private PageInfo<UserFile> page;
    List<UserFile> floors;
}
