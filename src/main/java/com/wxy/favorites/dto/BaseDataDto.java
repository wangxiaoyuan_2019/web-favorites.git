package com.wxy.favorites.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author e-Xiaoyuan.Wang
 * @since 2022/12/27 20:50
 */
@Data
@Accessors(chain = true)
public class BaseDataDto {
    private Integer users;
    private Integer admins;
    private Integer blacks;
    private Integer feedbacks;
    private Long visits;
    private Long searches;
}
