package com.wxy.favorites.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserLoginDto {
    private String accessToken;
    private Boolean admin;
}
