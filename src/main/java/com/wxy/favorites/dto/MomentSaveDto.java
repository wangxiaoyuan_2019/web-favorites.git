package com.wxy.favorites.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class MomentSaveDto {

    @NotBlank(message = "内容不能为空")
    private String content;

    @NotBlank(message = "text不能为空")
    private String text;
}
