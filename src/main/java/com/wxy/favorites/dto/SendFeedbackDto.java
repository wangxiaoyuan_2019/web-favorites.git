package com.wxy.favorites.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author e-Xiaoyuan.Wang
 * @since 2022/12/8 13:26
 */
@Data
public class SendFeedbackDto {

    @NotNull(message = "id不能为空")
    private Integer id;

    @NotBlank(message = "内容不能为空")
    private String content;
}
