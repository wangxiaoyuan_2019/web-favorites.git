package com.wxy.favorites.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author e-Xiaoyuan.Wang
 * @since 2023/2/7 17:29
 */
@Data
public class FavoritesTemplateDto {
    @ExcelProperty("标题")
    private String name;

    @ExcelProperty("地址")
    private String url;
}
