package com.wxy.favorites.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.experimental.Accessors;

import java.nio.file.Path;

@Data
@Accessors(chain = true)
public class ShareFileDto {
    private String filename;

    @JsonIgnore
    private Path path;
}
