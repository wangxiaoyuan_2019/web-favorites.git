package com.wxy.favorites.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author e-Xiaoyuan.Wang
 * @since 2022/12/27 21:38
 */
@Data
@Accessors(chain = true)
public class UserDto {

    private String username;

    private String nickName;

    private Integer clickCount;

    private Integer clickCountPercent;

    private Integer onlineHour;

    private Integer onlineHourPercent;
}
