package com.wxy.favorites.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserCapacityDto {

    private Integer id;

    private Long capacity;

    private Long usedSize;

    private Integer rootCount;
}
