package com.wxy.favorites.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class FavoritesMoveDto {

    private Integer from;

    @NotNull(message = "分类ID不能为空")
    private Integer to;

    private Integer moveAll;

    private List<Integer> ids;
}
