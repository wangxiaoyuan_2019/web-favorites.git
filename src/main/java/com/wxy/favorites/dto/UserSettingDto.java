package com.wxy.favorites.dto;

import lombok.Data;

@Data
public class UserSettingDto {
    private Integer searchStyle;
    private Integer favoriteLink;
    private Integer smartAi;
}
