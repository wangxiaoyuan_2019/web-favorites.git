package com.wxy.favorites.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ClockNoticeDto {
    private Integer userId;
    private String content;
}
