package com.wxy.favorites.security;

import com.wxy.favorites.constant.PublicConstants;
import com.wxy.favorites.dao.UserRepository;
import com.wxy.favorites.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/***
 * <p>
 * Description: 加载用户信息处理
 * </p>
 * @author wangxiaoyuan
 * 2021年12月08日
 */
@Component
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Value("${admin.account}")
    private String adminAccount;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user != null && Objects.equals(user.getStatus(), 1)) {
            /*
             * 此处查询用户权限，方便后续接口权限校验
             * 若接口需要开启权限验证，则在对应接口上配置 @Secured("xxx")注解
             * */
            List<String> permissionList = Objects.equals(user.getIsAdmin(), 1)
                    ? Objects.equals(user.getUsername(), adminAccount)
                    ? PublicConstants.SUPER_ADMIN_PERMISSION_LIST
                    : PublicConstants.ADMIN_PERMISSION_LIST
                    : PublicConstants.USER_PERMISSION_LIST;
            List<GrantedAuthority> authorities = permissionList.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
            return new SecurityUser(user.getId(), user.getUsername(), user.getPassword(), authorities);
        }
        throw new UsernameNotFoundException("用户不存在");
    }
}
