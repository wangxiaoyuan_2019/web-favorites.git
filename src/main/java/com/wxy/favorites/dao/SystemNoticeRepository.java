package com.wxy.favorites.dao;

import com.wxy.favorites.entity.SystemNotice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface SystemNoticeRepository extends JpaRepository<SystemNotice, Integer>, JpaSpecificationExecutor<SystemNotice> {
    SystemNotice findTopBy();
}
