package com.wxy.favorites.dao;

import com.wxy.favorites.entity.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface FeedbackRepository extends JpaRepository<Feedback, Integer>, JpaSpecificationExecutor<Feedback> {
    Feedback findTopByAccountOrderByCreateTimeDesc(String account);

    Long countByIsRead(Integer isRead);
}
