package com.wxy.favorites.dao;

import com.wxy.favorites.entity.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer>, JpaSpecificationExecutor<Category> {

    /**
     * 查询用户下所有分类
     *
     * @param userId
     * @return
     */
    List<Category> findByUserId(Integer userId);

    /**
     * 查询用户下所有分类（排序）
     *
     * @param userId
     * @return
     */
    List<Category> findByUserId(Integer userId, Sort sort);

    /**
     * 分页查询
     *
     * @param userId
     * @param pageable 分页信息
     * @return
     */
    Page<Category> findByUserId(Integer userId, Pageable pageable);

    void deleteAllByUserId(Integer userId);

    Long countByUserId(Integer userId);

    @Query(nativeQuery = true, value = "select max(c.sort) from t_category c where c.user_id = :userId and c.is_system = 0")
    Integer findMaxSortByUserId(Integer userId);

    @Query("update Category c set c.sort = 0 where c.userId = :userId and c.isSystem = 0")
    @Modifying
    void resetSort(Integer userId);

    Category findByNameAndUserIdAndIsSystem(String name, Integer userId, Integer isSystem);
}
