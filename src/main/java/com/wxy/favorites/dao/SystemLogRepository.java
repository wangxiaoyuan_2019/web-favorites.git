package com.wxy.favorites.dao;

import com.wxy.favorites.entity.SystemLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface SystemLogRepository extends JpaRepository<SystemLog, Integer>, JpaSpecificationExecutor<SystemLog> {
}
