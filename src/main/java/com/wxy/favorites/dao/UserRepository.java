package com.wxy.favorites.dao;

import com.wxy.favorites.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {
    User findByUsername(String username);

    User findByEmail(String email);

    User findByUsernameOrEmail(String username, String email);

    User findByUsernameAndEmail(String username, String email);

    @Modifying
    @Query(nativeQuery = true, value = "update t_favorites set is_share = 1")
    void updateShareAll();

    User findByNickName(String name);

    Long countByIsAdmin(Integer isAdmin);

    Long countByStatus(Integer status);

    @Query("SELECT SUM(u.clickCount) FROM User u")
    Long sumClickCount();

    @Query("SELECT SUM(u.searchCount) FROM User u")
    Long sumSearchCount();

    List<User> findTop10ByIsAdminOrderByClickCountDesc(Integer isAdmin);

    List<User> findTop10ByIsAdminOrderByOnlineHourDesc(Integer isAdmin);

    @Query("SELECT u.email FROM User u WHERE u.isAdmin = 0")
    List<String> findUserEmails();
}
