package com.wxy.favorites.dao;

import com.wxy.favorites.entity.WebContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public interface WebContentRepository extends JpaRepository<WebContent, Integer>, JpaSpecificationExecutor<WebContent> {
    List<WebContent> findByCreateTimeBetween(Date start, Date end);
}
