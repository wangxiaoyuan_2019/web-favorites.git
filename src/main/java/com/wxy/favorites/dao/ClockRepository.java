package com.wxy.favorites.dao;

import com.wxy.favorites.entity.Clock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;


@Repository
public interface ClockRepository extends JpaRepository<Clock, Integer>, JpaSpecificationExecutor<Clock> {
    Page<Clock> findAllByUserId(Integer userId, Pageable pageable);

    List<Clock> findAllByTimeAndStatusAndType(String time, Integer status, Integer type);

    Collection<? extends Clock> findAllByTimeAndStatusAndTypeAndCycleLike(String time, Integer status, Integer type, String cycle);

    Long countByUserId(Integer userId);
}
