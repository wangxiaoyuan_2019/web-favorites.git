package com.wxy.favorites.converter;

import com.wxy.favorites.dto.MomentSaveDto;
import com.wxy.favorites.entity.Moment;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MomentConverter {

    MomentConverter INSTANCE = Mappers.getMapper(MomentConverter.class);

    Moment toMoment(MomentSaveDto dto);
}
