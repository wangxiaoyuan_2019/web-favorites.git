package com.wxy.favorites.constant;

/**
 * @author wangxiaoyuan
 * 2021/5/8 12:42
 **/
public interface LogConstants {

    String LOGIN_SUCCESS_MSG = "管理员：%s，登录成功。";
    String LOGIN_FAILED_MSG = "管理员：%s，登录失败。";
    String ACCOUNT_ENABLE_MSG = "管理员：%s，启用了账号：%s";
    String ACCOUNT_DISABLE_MSG = "管理员：%s，禁用了账号：%s";
    String NOTICE_SEND_MSG = "管理员：%s，向用户发送了邮件通知。邮箱：%s，内容：%s";
    String RESET_PWD_MSG = "管理员：%s，重置了用户密码，账号：%s";
    String NOTICE_DELETE_MSG = "管理员：%s，删除了系统公告，标题：%s，内容：%s";
    String NOTICE_SAVE_MSG = "管理员：%s，发布了系统公告，标题：%s，内容：%s";
    String LOGOUT_MSG = "管理员：%s，退出登录。";
    String FEEDBACK_SEND_MSG = "管理员：%s，回复用户反馈。账号：%s，内容：%s，回复：%s";
    String ADMIN_DELETE_MSG = "管理员：%s，删除了管理账号：%s";
    String ADMIN_SAVE_MSG = "管理员：%s，新增了管理账号：%s";
    String ADMIN_UPDATE_MSG = "管理员：%s，修改了管理账号：%s";
    String RESET_ADMIN_PWD_MSG = "管理员：%s，重置了管理员密码，账号：%s";
    String LOG_CLEAN_MSG = "管理员：%s，清除了日志";
    String FEEDBACK_DELETE_MSG = "管理员：%s，删除了一条反馈。账号：%s，内容：%s，回复：%s";
    String RESTART_MSG = "管理员：%s，重启服务";
    String SHUTDOWN_MSG = "管理员：%s，停止服务";
}
