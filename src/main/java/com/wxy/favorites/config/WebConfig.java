package com.wxy.favorites.config;

import com.wxy.favorites.interceptor.ApiLogInterceptor;
import com.wxy.favorites.interceptor.RateLimiterInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author e-Xiaoyuan.Wang
 * @since 2022/2/9 13:41
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Value("${api-log.enable:false}")
    private Boolean apiLogEnable;

    @Value("${rate-limiter.enable:false}")
    private Boolean rateLimiterEnable;

    /**
     * 所有Api接口
     */
    private final List<String> patterns = Arrays.asList("/category/**", "/favorites/**",
            "/file/**", "/login/**", "/memorandum/**",
            "/moment/**", "/password/**", "/quick-navigation/**",
            "/register/**", "/search/**", "/share/**",
            "/task/**", "/user/**");

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        if (rateLimiterEnable) {
            registry.addInterceptor(new RateLimiterInterceptor()).addPathPatterns(patterns);
        }
        if (apiLogEnable) {
            registry.addInterceptor(new ApiLogInterceptor()).addPathPatterns(patterns);
        }
    }
}
