package com.wxy.favorites.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * <p>
 * 自定义线程池
 * </p>
 *
 * @author e-Xiaoyuan.Wang
 * @since 2022/11/29 22:57
 */
@Configuration
public class ExecutorConfig {

    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setCorePoolSize(4);//核心线程池数
        pool.setMaxPoolSize(20); // 最大线程数
        pool.setQueueCapacity(100);//队列容量,当核心线程数达到最大时，新任务会放在队列中排队等待执行
        pool.setKeepAliveSeconds(60);//线程空闲时间
        pool.setAllowCoreThreadTimeOut(false);//核心线程会一直存活，即使没有任务需要执行。（默认false）时，核心线程会超时关闭
        pool.setThreadNamePrefix("myTask-");//线程前缀名称
        return pool;
    }
}
