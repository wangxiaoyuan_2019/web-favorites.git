package com.wxy.favorites.core;

import lombok.Data;

@Data
public class SortDto {
    private Integer firstId;
    private Integer secondId;
}
