package com.wxy.favorites.core;

/**
 * 业务类异常
 */
public class NoRollbackException extends BizException {
    public NoRollbackException(String message) {
        super(message);
    }
}
