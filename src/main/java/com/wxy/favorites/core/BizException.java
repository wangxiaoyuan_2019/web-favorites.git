package com.wxy.favorites.core;

/**
 * 业务类异常
 */
public class BizException extends RuntimeException {
    public BizException(String message) {
        super(message);
    }
}
