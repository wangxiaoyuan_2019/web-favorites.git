package com.wxy.favorites.core;

import com.wxy.favorites.constant.ErrorConstants;
import lombok.Data;

/**
 * 统一接口数据响应
 */
@Data
public class ApiResponse<T> {

    private Integer code;
    private String msg;
    private T data;

    private ApiResponse(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static <T> ApiResponse<T> ok() {
        return new ApiResponse<>(ErrorConstants.DEFAULT_SUCCESS_CODE, ErrorConstants.DEFAULT_SUCCESS_MSG, null);
    }

    public static <T> ApiResponse<T> ok(T data) {
        return new ApiResponse<>(ErrorConstants.DEFAULT_SUCCESS_CODE, ErrorConstants.DEFAULT_SUCCESS_MSG, data);
    }

    public static <T> ApiResponse<T> fail() {
        return new ApiResponse<>(ErrorConstants.DEFAULT_ERROR_CODE, ErrorConstants.DEFAULT_ERROR_MSG, null);
    }

    public static <T> ApiResponse<T> fail(String msg) {
        return new ApiResponse<>(ErrorConstants.DEFAULT_ERROR_CODE, msg, null);
    }


    public static <T> ApiResponse<T> fail(Integer code, String msg) {
        return new ApiResponse<>(code, msg, null);
    }

    public static <T> ApiResponse<T> fail(Integer code, String msg, T data) {
        return new ApiResponse<>(code, msg, data);
    }
}
