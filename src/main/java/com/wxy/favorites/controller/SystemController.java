package com.wxy.favorites.controller;

import com.wxy.favorites.service.SystemLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/system")
@Api(tags = "系统设置")
@Secured("system")
public class SystemController {

    @Autowired
    private SystemLogService systemLogService;

    @PostMapping("/restart")
    @ApiOperation(value = "重启服务")
    public void restart() {
        systemLogService.restart();
    }

    @PostMapping("/shutdown")
    @ApiOperation(value = "停止服务")
    public void shutdown() {
        systemLogService.shutdown();
    }

    @GetMapping("/config")
    @ApiOperation(value = "查询系统配置")
    public Boolean config() {
        return true;
    }
}
