package com.wxy.favorites.controller;

import com.wxy.favorites.config.AppConfig;
import com.wxy.favorites.core.PageInfo;
import com.wxy.favorites.dto.SendNoticeDto;
import com.wxy.favorites.entity.User;
import com.wxy.favorites.service.SystemLogService;
import com.wxy.favorites.service.UserService;
import com.wxy.favorites.util.EmailUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/account")
@Api(tags = "用户列表")
@Secured("account")
public class AccountController {

    @Value("${user.account}")
    private String userAccount;

    @Value("${user.password}")
    private String userPassword;

    @Autowired
    private EmailUtils emailUtils;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SystemLogService systemLogService;

    @Autowired
    private AppConfig appConfig;

    @PostMapping("/enable/{id}")
    @ApiOperation(value = "禁用启用")
    public Integer enable(@PathVariable Integer id) {
        return userService.enableUser(id);
    }

    @PostMapping("/notice")
    @ApiOperation(value = "发送邮件")
    public Boolean notice(@RequestBody @Valid SendNoticeDto dto) {
        return userService.sendNotice(dto);
    }

    @PostMapping("/password/{id}")
    @ApiOperation(value = "重置密码")
    public Integer password(@PathVariable Integer id) {
        return userService.setUserPwd(id);
    }

    @GetMapping("/list")
    @ApiOperation(value = "查询用户列表")
    public PageInfo<User> list(@RequestParam(required = false) String name, @RequestParam(required = false) Integer pageNum, @RequestParam(required = false) Integer pageSize) {
        return userService.findUserPageList(name, pageNum, pageSize);
    }
}
