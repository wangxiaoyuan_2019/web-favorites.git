package com.wxy.favorites.controller;

import com.wxy.favorites.config.AppConfig;
import com.wxy.favorites.constant.PublicConstants;
import com.wxy.favorites.core.ApiResponse;
import com.wxy.favorites.core.PageInfo;
import com.wxy.favorites.entity.SearchType;
import com.wxy.favorites.security.ContextUtils;
import com.wxy.favorites.security.SecurityUser;
import com.wxy.favorites.service.SearchTypeService;
import com.wxy.favorites.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URL;

/**
 * @author wangxiaoyuan
 * 2021/1/5 11:25
 **/
@RestController
@RequestMapping("/search")
@Slf4j
@Api(tags = "搜索引擎")
@Secured("search")
public class SearchController {

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private SearchTypeService searchTypeService;

    @Autowired
    private UserService userService;

    @GetMapping("/data")
    @ApiOperation(value = "查询搜索引擎")
    public PageInfo<SearchType> data(@RequestParam Integer pageNum, @RequestParam Integer pageSize) {
        SecurityUser user = ContextUtils.getCurrentUser();
        return searchTypeService.findPageByUserId(user.getId(), pageNum, pageSize);
    }

    @PostMapping
    @ApiOperation(value = "新增搜索引擎")
    public void save(@RequestBody SearchType searchType) {
        searchTypeService.saveSearch(searchType);
    }

    @GetMapping("/delete/{id}")
    @ApiOperation(value = "删除搜索引擎")
    public void delete(@PathVariable Integer id) {
        searchTypeService.deleteById(id);
    }

    @GetMapping("/query/{id}")
    @ApiOperation(value = "根据id查询")
    public SearchType query(@PathVariable Integer id) {
        return searchTypeService.findById(id);
    }

}
