package com.wxy.favorites.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.wxy.favorites.constant.LogConstants;
import com.wxy.favorites.constant.PublicConstants;
import com.wxy.favorites.core.ApiResponse;
import com.wxy.favorites.core.PageInfo;
import com.wxy.favorites.entity.SystemLog;
import com.wxy.favorites.security.ContextUtils;
import com.wxy.favorites.security.SecurityUser;
import com.wxy.favorites.service.SystemLogService;
import com.wxy.favorites.util.IpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/log")
@Api(tags = "系统日志")
@Secured("log")
public class LogController {

    @Autowired
    private SystemLogService systemLogService;

    @GetMapping("/list")
    @ApiOperation(value = "查询日志列表")
    public PageInfo<SystemLog> list(@RequestParam(required = false) String content,
                            @RequestParam(required = false) String ip,
                            @RequestParam(required = false) String time,
                            @RequestParam(required = false) Integer pageNum,
                            @RequestParam(required = false) Integer pageSize) {
        Date startTime = null;
        Date endTime = null;
        if (StrUtil.isNotBlank(time)) {
            String[] split = time.split(PublicConstants.DEFAULT_DELIMITER);
            startTime = DateUtil.parse(split[0].trim());
            endTime = DateUtil.parse(split[1].trim());
        }
        return systemLogService.findPage(content, ip, startTime, endTime, pageNum, pageSize);
    }

    @PostMapping("/clean")
    @ApiOperation(value = "清空日志")
    public Boolean clean() {
        return systemLogService.clean();
    }
}
