package com.wxy.favorites.controller;

import com.wxy.favorites.config.AppConfig;
import com.wxy.favorites.entity.SystemNotice;
import com.wxy.favorites.service.SystemLogService;
import com.wxy.favorites.service.SystemNoticeService;
import com.wxy.favorites.service.UserService;
import com.wxy.favorites.util.EmailUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/notice")
@Api(tags = "系统公告")
@Secured("notice")
public class NoticeController {

    @Autowired
    private SystemNoticeService systemNoticeService;

    @Autowired
    private SystemLogService systemLogService;

    @Autowired
    private EmailUtils emailUtils;

    @Autowired
    private UserService userService;

    @Autowired
    private AppConfig appConfig;

    @PostMapping("/delete")
    @ApiOperation(value = "删除公告")
    public void delete() {
        systemNoticeService.delete();
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存")
    public SystemNotice save(@RequestBody SystemNotice notice) {
        return systemNoticeService.saveNotice(notice);
    }

    @GetMapping("/get")
    @ApiOperation(value = "查询")
    public SystemNotice get() {
        return systemNoticeService.getSystemNotice();
    }
}
