package com.wxy.favorites.controller;

import com.wxy.favorites.entity.Password;
import com.wxy.favorites.service.FavoritesService;
import com.wxy.favorites.service.PasswordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/password")
@Api(tags = "密码")
@Secured("password")
public class PasswordController {

    @Autowired
    private PasswordService passwordService;

    @Autowired
    private FavoritesService favoritesService;

    @PostMapping
    @ApiOperation(value = "保存密码")
    public Integer save(@RequestBody Password password) {
        return passwordService.savePwd(password);
    }

    @GetMapping("/fid/{fid}")
    @ApiOperation(value = "根据收藏id查询")
    public Password queryByFavoritesId(@PathVariable Integer fid) {
        return passwordService.findByFavoritesId(fid);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除密码")
    public void delete(@PathVariable Integer id) {
        passwordService.deleteById(id);
    }
}
