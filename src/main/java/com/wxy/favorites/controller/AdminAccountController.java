package com.wxy.favorites.controller;

import com.wxy.favorites.core.PageInfo;
import com.wxy.favorites.entity.User;
import com.wxy.favorites.service.SystemLogService;
import com.wxy.favorites.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin-account")
@Api(tags = "管理员列表")
@Secured("admin-account")
public class AdminAccountController {

    @Value("${admin.account}")
    private String adminAccount;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SystemLogService systemLogService;


    @PostMapping("/delete/{id}")
    @ApiOperation(value = "删除账号")
    public Boolean delete(@PathVariable Integer id) {
        return userService.delete(id);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存")
    public User save(@RequestBody User user) {
        if (user.getId() == null) {
            userService.saveUser(user);
        } else {
            userService.updateUser(user);
        }
        return user;
    }

    @PostMapping("/enable/{id}")
    @ApiOperation(value = "禁用启用")
    public Integer enable(@PathVariable Integer id) {
        return userService.enableAdmin(id);
    }

    @PostMapping("/password")
    @ApiOperation(value = "重置密码")
    public Integer password(@RequestParam Integer id, @RequestParam String pwd) {
        return userService.setAdminPwd(id, pwd);
    }

    @GetMapping("/list")
    @ApiOperation(value = "查询用户列表")
    public PageInfo<User> list(@RequestParam(required = false) String name, @RequestParam(required = false) Integer pageNum, @RequestParam(required = false) Integer pageSize) {
        return userService.findAdminPageList(name, pageNum, pageSize);
    }

    @GetMapping("/username/{username}")
    @ApiOperation(value = "查询用户名是否存在")
    public Boolean existUsername(@PathVariable String username) {
        return userService.existUsername(username);
    }

    @GetMapping("/email/{email}")
    @ApiOperation(value = "查询邮箱是否存在")
    public Boolean existEmail(@PathVariable String email) {
        return userService.existEmail(email);
    }
}
