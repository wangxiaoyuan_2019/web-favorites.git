package com.wxy.favorites.controller;

import com.wxy.favorites.config.AppConfig;
import com.wxy.favorites.entity.QuickNavigation;
import com.wxy.favorites.security.ContextUtils;
import com.wxy.favorites.service.QuickNavigationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/***
 * <p>
 * Description: 描述
 * </p>
 * @author wangxiaoyuan
 * 2021年11月25日
 */
@RestController
@RequestMapping("/quick-navigation")
@Slf4j
@Api(tags = "快捷导航")
@Secured("navigation")
public class QuickNavigationController {

    @Autowired
    private QuickNavigationService quickNavigationService;

    @Autowired
    private AppConfig appConfig;

    @PostMapping
    @ApiOperation(value = "新增快捷导航")
    public void save(@RequestBody QuickNavigation quickNavigation) {
        quickNavigationService.saveNav(quickNavigation);
    }

    @PostMapping("/sort")
    @ApiOperation(value = "排序")
    public void sort(@RequestBody List<QuickNavigation> dto) {
        quickNavigationService.sort(dto);
    }

    @GetMapping("/list")
    @ApiOperation(value = "查询快捷导航")
    public List<QuickNavigation> findList() {
        Integer userId = ContextUtils.getCurrentUser().getId();
        return quickNavigationService.findByUserId(userId);
    }

    @GetMapping("/delete/{id}")
    @ApiOperation(value = "删除快捷导航")
    public void delete(@PathVariable Integer id) {
        quickNavigationService.deleteById(id);
    }
}
