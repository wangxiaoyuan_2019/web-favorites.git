package com.wxy.favorites.controller;

import com.wxy.favorites.config.AppConfig;
import com.wxy.favorites.entity.User;
import com.wxy.favorites.service.CategoryService;
import com.wxy.favorites.service.FavoritesService;
import com.wxy.favorites.service.UserService;
import com.wxy.favorites.service.VerificationService;
import com.wxy.favorites.util.EmailUtils;
import com.wxy.favorites.util.TokenUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/register")
@Api(tags = "注册")
public class RegisterController {

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private UserService userService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private FavoritesService favoritesService;

    @Autowired
    private EmailUtils emailUtils;

    @Autowired
    private VerificationService verificationService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TokenUtils tokenUtils;

    /**
     * 注册
     *
     * @param user
     * @return
     */
    @PostMapping
    @ApiOperation(value = "用户注册")
    public String register(@RequestBody User user) {
        return userService.registerUser(user);
    }

    @GetMapping("/{username}")
    @ApiOperation(value = "查询用户名是否存在")
    public Boolean checkUsername(@PathVariable String username) {
        return userService.existUsername(username);
    }

    @GetMapping("/email/code")
    @ApiOperation(value = "邮箱注册-获取验证码")
    public void code(@RequestParam String email) {
        userService.registerCode(email);
    }

    @GetMapping("/email/{email}")
    @ApiOperation(value = "验证邮箱是否存在")
    public Boolean existEmail(@PathVariable String email) {
        return userService.existEmail(email);
    }
}
