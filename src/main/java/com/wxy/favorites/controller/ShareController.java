package com.wxy.favorites.controller;

import com.wxy.favorites.core.PageInfo;
import com.wxy.favorites.entity.Favorites;
import com.wxy.favorites.security.ContextUtils;
import com.wxy.favorites.service.FavoritesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/share")
@Api(tags = "书签库")
@Secured("share")
public class ShareController {

    @Autowired
    private FavoritesService favoritesService;

    @GetMapping("/list")
    @ApiOperation(value = "查询书签库")
    public PageInfo<Favorites> list(@RequestParam Integer pageNum, @RequestParam Integer pageSize, @RequestParam(required = false) String name,@RequestParam(required = false) Integer sortType) {
        return favoritesService.findShareList(name, pageNum, pageSize,sortType);
    }

    @GetMapping("/support/{id}")
    @ApiOperation(value = "搜藏书签")
    public Integer support(@PathVariable Integer id) {
        return favoritesService.saveSupport(ContextUtils.getCurrentUser().getId(), id);
    }

    @GetMapping("/click/{id}")
    @ApiOperation(value = "点击书签")
    public Integer click(@PathVariable Integer id) {
        return favoritesService.clickCountAdd(id);
    }
}
