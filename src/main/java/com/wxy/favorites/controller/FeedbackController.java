package com.wxy.favorites.controller;

import com.wxy.favorites.core.PageInfo;
import com.wxy.favorites.dto.SendFeedbackDto;
import com.wxy.favorites.entity.Feedback;
import com.wxy.favorites.service.FeedbackService;
import com.wxy.favorites.service.SystemLogService;
import com.wxy.favorites.service.UserService;
import com.wxy.favorites.util.EmailUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/feedback")
@Api(tags = "系统反馈")
@Secured("feedback")
public class FeedbackController {

    @Autowired
    private FeedbackService feedbackService;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailUtils emailUtils;

    @Autowired
    private SystemLogService systemLogService;

    @GetMapping("/list")
    @ApiOperation(value = "查询反馈列表")
    public PageInfo<Feedback> list(@RequestParam(required = false) String account, @RequestParam(required = false) String content, @RequestParam(required = false) Integer isRead, @RequestParam(required = false) Integer pageNum, @RequestParam(required = false) Integer pageSize) {
        return feedbackService.findPage(account, content, isRead, pageNum, pageSize);
    }

    @PostMapping("/read/{id}")
    @ApiOperation(value = "已读")
    public Integer read(@PathVariable Integer id) {
        return feedbackService.read(id);
    }

    @PostMapping("/notice")
    @ApiOperation(value = "发送邮件")
    public Boolean notice(@RequestBody @Valid SendFeedbackDto dto) {
        return feedbackService.notice(dto);
    }

    @PostMapping("/delete/{id}")
    @ApiOperation(value = "删除反馈")
    public Boolean delete(@PathVariable Integer id) {
        return feedbackService.delete(id);
    }
}
