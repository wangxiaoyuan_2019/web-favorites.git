package com.wxy.favorites.controller;

import com.wxy.favorites.config.AppConfig;
import com.wxy.favorites.dto.UserInfoUpdateDto;
import com.wxy.favorites.dto.UserSettingDto;
import com.wxy.favorites.entity.Feedback;
import com.wxy.favorites.entity.SystemNotice;
import com.wxy.favorites.entity.User;
import com.wxy.favorites.security.ContextUtils;
import com.wxy.favorites.security.SecurityUser;
import com.wxy.favorites.service.FeedbackService;
import com.wxy.favorites.service.SystemNoticeService;
import com.wxy.favorites.service.UserService;
import com.wxy.favorites.service.VerificationService;
import com.wxy.favorites.util.EmailUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/user")
@Api(tags = "用户")
@Secured("user")
public class UserController {

    @Value("${user.account}")
    private String userAccount;

    @Autowired
    private SystemNoticeService systemNoticeService;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailUtils emailUtils;

    @Autowired
    private VerificationService verificationService;

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private FeedbackService feedbackService;

    @GetMapping("/info")
    @ApiOperation(value = "查询登录信息")
    public User info() {
        return userService.authInfo();
    }

    @PostMapping("/resetSort")
    @ApiOperation(value = "重置排序")
    public Boolean resetSort() {
        SecurityUser securityUser = ContextUtils.getCurrentUser();
        return userService.resetSort(securityUser.getId());
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存信息")
    public void save(@RequestBody User user) {
        userService.saveNickName(user);
    }

    @PostMapping("/feedback")
    @ApiOperation(value = "反馈")
    public void feedback(@RequestBody Feedback feedback) {
        userService.feedback(feedback);
    }

    @GetMapping("/notice")
    @ApiOperation(value = "查询系统公告")
    public SystemNotice notice() {
        return systemNoticeService.getSystemNotice();
    }

    @GetMapping("/nickname/check/{name}")
    @ApiOperation(value = "检查昵称重复")
    public Boolean nickname(@PathVariable String name) {
        return userService.existNickName(name);
    }

    @PostMapping("/cleanData")
    @ApiOperation(value = "清除所有数据")
    public void cleanData(@RequestParam String loginPwd) {
        userService.cleanData(loginPwd);
    }

    @PostMapping("/password")
    @ApiOperation(value = "重置密码")
    public void password(@RequestParam String code, @RequestParam String newPassword) {
        userService.resetPwd(code,newPassword);
    }

    @PostMapping("/style")
    @ApiOperation(value = "保存浏览模式")
    public void viewStyle(@RequestParam Integer viewStyle) {
        userService.updateViewStyle(viewStyle);
    }

    @GetMapping("/email/code")
    @ApiOperation(value = "修改个人信息-获取验证码")
    public void code(@RequestParam String email) {
        userService.updateUserInfoCode(email);
    }

    @GetMapping("/pwd/email/code")
    @ApiOperation(value = "修改密码-获取验证码")
    public void pwdCode() {
        userService.pwdCode();
    }

    @PostMapping("/update")
    @ApiOperation(value = "修改个人信息")
    public Boolean update(@RequestBody @Valid UserInfoUpdateDto dto) {
        return userService.updateUserInfo(dto);
    }

    @PostMapping("/setting")
    @ApiOperation(value = "修改设置")
    public void setting(@RequestBody UserSettingDto dto) {
        userService.updateSettings(dto);
    }

    @GetMapping("/data")
    @ApiOperation(value = "获取统计信息")
    public Map<String, Object> getUserData() {
        SecurityUser user = ContextUtils.getCurrentUser();
        return userService.findUserData(user.getId());
    }

    @GetMapping("/online")
    @ApiOperation(value = "增加在线时长")
    public void online() {
        userService.onlineAdd();
    }

    @GetMapping("/search")
    @ApiOperation(value = "增加搜索次数")
    public void search() {
        userService.searchAdd();
    }

    @GetMapping("/visit")
    @ApiOperation(value = "增加访问次数")
    public void visit() {
        userService.visitAdd();
    }
}
