package com.wxy.favorites.controller;

import com.wxy.favorites.constant.PublicConstants;
import com.wxy.favorites.core.ApiResponse;
import com.wxy.favorites.core.PageInfo;
import com.wxy.favorites.dto.MomentSaveDto;
import com.wxy.favorites.dto.MomentUpdateDto;
import com.wxy.favorites.entity.Moment;
import com.wxy.favorites.security.ContextUtils;
import com.wxy.favorites.security.SecurityUser;
import com.wxy.favorites.service.MomentService;
import com.wxy.favorites.util.AssertUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/moment")
@Api(tags = "瞬间")
@Secured("moment")
public class MomentController {

    @Autowired
    private MomentService momentService;

    @PostMapping
    @ApiOperation(value = "新增瞬间")
    public Integer save(@RequestBody @Valid MomentSaveDto dto) {
        return momentService.saveMoment(dto);
    }

    @PostMapping("/update")
    @ApiOperation(value = "修改瞬间")
    public Boolean update(@RequestBody @Valid MomentUpdateDto dto) {
        return momentService.updateMoment(dto);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "根据id查询")
    public Moment query(@PathVariable Integer id) {
        return momentService.findById(id);
    }

    @GetMapping("/count")
    @ApiOperation(value = "统计我的瞬间")
    public Integer count() {
        SecurityUser user = ContextUtils.getCurrentUser();
        return momentService.countByUserId(user.getId());
    }

    @GetMapping("/top")
    @ApiOperation(value = "查询置顶瞬间")
    public Moment queryTop() {
        SecurityUser user = ContextUtils.getCurrentUser();
        return momentService.findTopMoment(user.getId());
    }

    @PostMapping("/top/{id}")
    @ApiOperation(value = "置顶")
    public Boolean setTop(@PathVariable Integer id) {
        return momentService.setTop(id);
    }

    @DeleteMapping("/top/{id}")
    @ApiOperation(value = "取消置顶")
    public Boolean cancelTop(@PathVariable Integer id) {
        return momentService.cancelTop(id);
    }

    @GetMapping("/search")
    @ApiOperation(value = "搜索瞬间")
    public List<Moment> search(@RequestParam String text) {
        SecurityUser user = ContextUtils.getCurrentUser();
        return momentService.findMoment(user.getId(), text);
    }

    @GetMapping("/delete/{id}")
    @ApiOperation(value = "删除瞬间")
    public Boolean delete(@PathVariable Integer id) {
        return momentService.deleteById(id);
    }

    @GetMapping("/list")
    @ApiOperation(value = "分页查询")
    public PageInfo<Moment> list(@RequestParam(required = false) String text, @RequestParam Integer pageNum, @RequestParam Integer pageSize) {
        SecurityUser user = ContextUtils.getCurrentUser();
        return momentService.findPageByUserIdAndTextLike(user.getId(), text, pageNum, pageSize);
    }

}
