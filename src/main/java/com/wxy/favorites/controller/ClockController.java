package com.wxy.favorites.controller;

import com.wxy.favorites.core.PageInfo;
import com.wxy.favorites.entity.Clock;
import com.wxy.favorites.entity.Task;
import com.wxy.favorites.security.ContextUtils;
import com.wxy.favorites.security.SecurityUser;
import com.wxy.favorites.service.ClockService;
import com.wxy.favorites.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/clock")
@Api(tags = "闹钟")
@Secured("clock")
public class ClockController {

    @Autowired
    private ClockService clockService;

    @PostMapping
    @ApiOperation(value = "新增闹钟")
    public void save(@RequestBody Clock clock) {
        clockService.save(clock);
    }

    @GetMapping("/delete/{id}")
    @ApiOperation(value = "删除闹钟")
    public void delete(@PathVariable Integer id) {
        clockService.deleteById(id);
    }

    @GetMapping("/enable/{id}")
    @ApiOperation(value = "开启关闭")
    public Integer enable(@PathVariable Integer id) {
        return clockService.enable(id);
    }

    @GetMapping("/list")
    @ApiOperation(value = "查询列表")
    public PageInfo<Clock> findPageList(@RequestParam Integer pageNum, @RequestParam Integer pageSize) {
        SecurityUser user = ContextUtils.getCurrentUser();
        return clockService.findPageByUserId(user.getId(), pageNum, pageSize);
    }

}
