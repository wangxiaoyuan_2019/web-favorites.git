package com.wxy.favorites.controller;

import com.wxy.favorites.entity.User;
import com.wxy.favorites.service.SystemLogService;
import com.wxy.favorites.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
@Api(tags = "管理员")
@Secured("admin")
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private SystemLogService systemLogService;


    @GetMapping("/info")
    @ApiOperation(value = "查询登录信息")
    public User info() {
        return userService.authInfo();
    }

    @GetMapping("/logout")
    @ApiOperation(value = "退出")
    public Boolean logout() {
        return userService.logout();
    }
}
