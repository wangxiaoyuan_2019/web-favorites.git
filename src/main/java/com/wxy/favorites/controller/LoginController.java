package com.wxy.favorites.controller;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.wxy.favorites.config.AppConfig;
import com.wxy.favorites.constant.EmailConstants;
import com.wxy.favorites.constant.ErrorConstants;
import com.wxy.favorites.constant.LogConstants;
import com.wxy.favorites.constant.PublicConstants;
import com.wxy.favorites.core.ApiResponse;
import com.wxy.favorites.dto.UserLoginDto;
import com.wxy.favorites.entity.*;
import com.wxy.favorites.service.*;
import com.wxy.favorites.util.*;
import com.wxy.favorites.websocket.WebSocketServer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/login")
@Slf4j
@Api(tags = "登录")
public class LoginController {

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private UserService userService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private FavoritesService favoritesService;

    @Autowired
    private EmailUtils emailUtils;

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private VerificationService verificationService;

    @Value("${captcha.random:true}")
    private Boolean randomCaptcha;

    @Value("${user.account}")
    private String userAccount;

    @Value("${admin.account}")
    private String adminAccount;

    @Autowired
    private SystemLogService systemLogService;

    @GetMapping("/email/code")
    @ApiOperation(value = "邮箱登录-获取验证码")
    public void code(@RequestParam String email) {
        userService.emailLoginCode(email);
    }

    @PostMapping("/emailLogin")
    @ApiOperation(value = "邮箱登录")
    public UserLoginDto emailLogin(@RequestParam String email, @RequestParam String code) {
        return userService.emailLogin(email, code);
    }

    @PostMapping
    @ApiOperation(value = "账号密码登录")
    public UserLoginDto login(@RequestBody User user, @RequestParam(required = false) String remember) {
        return userService.login(user, remember);
    }

    @PostMapping("/qrLogin")
    @ApiOperation(value = "扫码登录")
    public UserLoginDto qrLogin(@RequestBody User user) {
        return userService.qrLogin(user);
    }


    @GetMapping("/captcha")
    @ApiOperation(value = "账号密码登录-获取验证码")
    public Map<String, String> captcha() {
        return randomCaptcha ? CaptchaUtils.generateRandom() : CaptchaUtils.generate();
    }

    @GetMapping("/forgot/code")
    @ApiOperation(value = "忘记密码-获取验证码")
    public void forgotCode(@RequestParam String email) {
        userService.forgotCode(email);
    }

    @PostMapping("/forgot")
    @ApiOperation(value = "忘记密码")
    public void forgot(@RequestBody User user) {
        userService.forgot(user);
    }
}
