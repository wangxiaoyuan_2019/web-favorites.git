package com.wxy.favorites.controller;

import com.wxy.favorites.dto.BaseDataDto;
import com.wxy.favorites.dto.UserDto;
import com.wxy.favorites.entity.WebContent;
import com.wxy.favorites.service.StatisticService;
import com.wxy.favorites.service.WebContentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/statistic")
@Api(tags = "统计")
@Secured("statistic")
public class StatisticController {

    @Autowired
    private StatisticService statisticService;

    @Autowired
    private WebContentService webContentService;

    @GetMapping("/baseData")
    @ApiOperation(value = "基础数据查询")
    public BaseDataDto baseData() {
        return statisticService.baseData();
    }

    @GetMapping("/visitTop10")
    @ApiOperation(value = "访问次数top10")
    public List<UserDto> visitTop10() {
        return statisticService.visitTop10();
    }

    @GetMapping("/onlineTop10")
    @ApiOperation(value = "在线时长top10")
    public List<UserDto> onlineTop10() {
        return statisticService.onlineTop10();
    }

    @GetMapping("/contentData")
    @ApiOperation(value = "内容统计")
    public WebContent contentData() {
        return statisticService.contentData();
    }

    @GetMapping("/contentOverview/{type}")
    @ApiOperation(value = "内容概览")
    public List<WebContent> contentOverview(@PathVariable Integer type) {
        return webContentService.findAll(type);
    }
}
