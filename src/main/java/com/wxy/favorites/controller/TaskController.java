package com.wxy.favorites.controller;

import com.wxy.favorites.core.PageInfo;
import com.wxy.favorites.entity.Task;
import com.wxy.favorites.security.ContextUtils;
import com.wxy.favorites.security.SecurityUser;
import com.wxy.favorites.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/task")
@Api(tags = "日程")
@Secured("task")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @PostMapping
    @ApiOperation(value = "新增日程")
    public void save(@RequestBody Task task) {
        taskService.saveTask(task);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "根据id查询")
    public Task query(@PathVariable Integer id) {
        return taskService.findById(id);
    }

    @GetMapping("/delete/{id}")
    @ApiOperation(value = "删除日程")
    public void delete(@PathVariable Integer id) {
        taskService.deleteById(id);
    }

    @GetMapping("/clean/{date}")
    @ApiOperation(value = "清空日程")
    public void cleanByDate(@PathVariable String date) throws ParseException {
        SecurityUser user = ContextUtils.getCurrentUser();
        taskService.deleteAllByDate(user.getId(), date);
    }

    @GetMapping("/done/{id}")
    @ApiOperation(value = "完成")
    public void done(@PathVariable Integer id) {
        taskService.done(id);
    }

    @GetMapping("/cancel/{id}")
    @ApiOperation(value = "取消")
    public void cancel(@PathVariable Integer id) {
        taskService.cancel(id);
    }

    @GetMapping("/all/{date}")
    @ApiOperation(value = "日程统计")
    public List<Map<String, Object>> findAll(@PathVariable String date) throws ParseException {
        SecurityUser user = ContextUtils.getCurrentUser();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.parseInt(date.substring(0, 4)));
        calendar.set(Calendar.MONTH, Integer.parseInt(date.substring(5, 7)) - 1);
        int lastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        return taskService.countByDayBetween(user.getId(), date + "-01", date + "-" + lastDay);
    }

    @GetMapping("/todo")
    @ApiOperation(value = "查询待办")
    public List<Map<String, Object>> todo() {
        SecurityUser user = ContextUtils.getCurrentUser();
        return taskService.findTodoOfDay(user.getId());
    }

    @GetMapping("/list")
    @ApiOperation(value = "查询日程列表")
    public PageInfo<Task> findPageList(@RequestParam String date, @RequestParam Integer pageNum, @RequestParam Integer pageSize) throws ParseException {
        SecurityUser user = ContextUtils.getCurrentUser();
        return taskService.findPageByUserIdAndTaskDate(user.getId(), date, pageNum, pageSize);
    }

}
