package com.wxy.favorites.service;

import cn.hutool.core.date.DateUtil;
import com.wxy.favorites.core.NoRollbackException;
import com.wxy.favorites.dao.WebContentRepository;
import com.wxy.favorites.entity.WebContent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @Author wangxiaoyuan
 * @Date 2020/4/24 11:50
 * @Description
 **/
@Slf4j
@Service
@Transactional(noRollbackFor = NoRollbackException.class)
public class WebContentService {

    @Autowired
    private WebContentRepository webContentRepository;

    public WebContent save(WebContent webContent) {
        return webContentRepository.save(webContent);
    }

    public List<WebContent> findAll(int type) {
        Date end = new Date();
        Date start = type == 1 ? DateUtil.offsetWeek(end, -1) : type == 2 ? DateUtil.offsetMonth(end, -1) : type == 3 ? DateUtil.offsetMonth(end, -12) : end;
        return webContentRepository.findByCreateTimeBetween(start, end);
    }
}

