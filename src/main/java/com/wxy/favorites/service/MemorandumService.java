package com.wxy.favorites.service;

import com.wxy.favorites.core.NoRollbackException;
import com.wxy.favorites.dao.MemorandumRepository;
import com.wxy.favorites.entity.Memorandum;
import com.wxy.favorites.core.PageInfo;
import com.wxy.favorites.security.ContextUtils;
import com.wxy.favorites.security.SecurityUser;
import com.wxy.favorites.util.AssertUtils;
import com.wxy.favorites.util.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author wangxiaoyuan
 * @Date 2020/4/24 11:50
 * @Description
 **/
@Slf4j
@Service
@Transactional(noRollbackFor = NoRollbackException.class)
public class MemorandumService {

    @Autowired
    private MemorandumRepository memorandumRepository;

    public Memorandum save(Memorandum memorandum) {
        return memorandumRepository.save(memorandum);
    }

    public Memorandum findById(Integer id) {
        return memorandumRepository.findById(id).orElse(null);
    }

    public void deleteById(Integer id) {
        memorandumRepository.deleteById(id);
    }

    public PageInfo<Memorandum> findPageByUserIdAndContentLike(Integer userId, String content, Integer pageNum, Integer pageSize) {
        List<Sort.Order> orders = new ArrayList<>();
        orders.add(new Sort.Order(Sort.Direction.DESC, "createTime"));
        orders.add(new Sort.Order(Sort.Direction.DESC, "id"));
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize, Sort.by(orders));
        Page<Memorandum> page = memorandumRepository.findPageByUserIdAndContentLike(userId, "%" + SqlUtils.trimAndEscape(content) + "%", pageable);
        return new PageInfo<>(page.getContent(), page.getTotalPages(), page.getTotalElements());
    }

    public List<Memorandum> findByUserId(Integer userId) {
        return memorandumRepository.findByUserId(userId);
    }

    public long countByUserId(Integer userId) {
       return memorandumRepository.countByUserId(userId);
    }

    public List<Memorandum> findMemorandum(Integer userId, String content) {
        content = SqlUtils.trimAndEscape(content);
        List<Sort.Order> orders = new ArrayList<>();
        orders.add(new Sort.Order(Sort.Direction.DESC, "createTime"));
        orders.add(new Sort.Order(Sort.Direction.DESC, "id"));
        return memorandumRepository.findByUserIdAndContentLike(userId, "%" + content + "%", Sort.by(orders));
    }

    public Memorandum update(Memorandum memorandum) {
        return memorandumRepository.save(memorandum);
    }

    public void saveMemo(Memorandum memorandum) {
        AssertUtils.notBlank(memorandum.getContent(),"内容不能为空");
        SecurityUser user = ContextUtils.getCurrentUser();
        memorandum.setUserId(user.getId());
        memorandum.setCreateTime(new Date());
        memorandumRepository.save(memorandum);
    }

    public void updateMemo(Memorandum memorandum) {
        AssertUtils.notBlank(memorandum.getContent(),"内容不能为空");
        Memorandum memorandum1 = memorandumRepository.findById(memorandum.getId()).orElse(null);
        AssertUtils.notNull(memorandum1, "备忘录不存在");
        memorandum1.setContent(memorandum.getContent());
        memorandum1.setCreateTime(new Date());
        memorandumRepository.save(memorandum1);
    }
}

