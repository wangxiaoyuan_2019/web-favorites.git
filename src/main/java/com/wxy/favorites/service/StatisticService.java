package com.wxy.favorites.service;

import com.wxy.favorites.core.NoRollbackException;
import com.wxy.favorites.dao.*;
import com.wxy.favorites.dto.BaseDataDto;
import com.wxy.favorites.dto.UserDto;
import com.wxy.favorites.entity.User;
import com.wxy.favorites.entity.WebContent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *
 * </p>
 *
 * @author e-Xiaoyuan.Wang
 * @since 2022/12/27 21:20
 */
@Slf4j
@Service
@Transactional(noRollbackFor = NoRollbackException.class)
public class StatisticService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FavoritesRepository favoritesRepository;

    @Autowired
    private SearchTypeRepository searchTypeRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private MemorandumRepository memorandumRepository;

    @Autowired
    private UserFileRepository userFileRepository;

    @Autowired
    private MomentRepository momentRepository;

    @Autowired
    private FeedbackRepository feedbackRepository;

    public BaseDataDto baseData() {
        return new BaseDataDto()
                .setUsers(userRepository.countByIsAdmin(0).intValue())
                .setAdmins(userRepository.countByIsAdmin(1).intValue())
                .setBlacks(userRepository.countByStatus(3).intValue())
                .setFeedbacks(feedbackRepository.countByIsRead(0).intValue())
                .setVisits(userRepository.sumClickCount())
                .setSearches(userRepository.sumSearchCount());
    }

    public List<UserDto> visitTop10() {
        List<User> list = userRepository.findTop10ByIsAdminOrderByClickCountDesc(0);
        int first = list.size() > 0 ? list.get(0).getClickCount() : 0;
        return list.stream().map(user -> new UserDto().setUsername(user.getUsername()).setNickName(user.getNickName())
                .setClickCount(user.getClickCount())
                .setClickCountPercent(first == 0 ? 0 : Math.floorDiv(user.getClickCount() * 100, first))).collect(Collectors.toList());
    }

    public List<UserDto> onlineTop10() {
        List<User> list = userRepository.findTop10ByIsAdminOrderByOnlineHourDesc(0);
        int first = list.size() > 0 ? list.get(0).getOnlineHour() + 1 : 0;
        return list.stream().map(user -> new UserDto().setUsername(user.getUsername()).setNickName(user.getNickName())
                .setOnlineHour(user.getOnlineHour() + 1)
                .setOnlineHourPercent(first == 0 ? 0 : Math.floorDiv((user.getOnlineHour() + 1) * 100, first))).collect(Collectors.toList());
    }

    public WebContent contentData() {
        return new WebContent()
                .setFavorites((int) favoritesRepository.count())
                .setMoments((int) momentRepository.count())
                .setSearchTypes((int) searchTypeRepository.count())
                .setTasks((int) taskRepository.count())
                .setMemorandums((int) memorandumRepository.count())
                .setShares(favoritesRepository.countByIsShare(1).intValue())
                .setFiles(userFileRepository.countByIsDir(0).intValue());
    }
}
