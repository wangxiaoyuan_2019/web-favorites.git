package com.wxy.favorites.service;

import com.google.common.collect.Lists;
import com.wxy.favorites.config.AppConfig;
import com.wxy.favorites.constant.EmailConstants;
import com.wxy.favorites.constant.LogConstants;
import com.wxy.favorites.constant.PublicConstants;
import com.wxy.favorites.core.ApiResponse;
import com.wxy.favorites.core.NoRollbackException;
import com.wxy.favorites.dao.SystemLogRepository;
import com.wxy.favorites.dao.SystemNoticeRepository;
import com.wxy.favorites.entity.SystemLog;
import com.wxy.favorites.entity.SystemNotice;
import com.wxy.favorites.security.ContextUtils;
import com.wxy.favorites.security.SecurityUser;
import com.wxy.favorites.util.EmailUtils;
import com.wxy.favorites.util.IpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;

/**
 * @Author wangxiaoyuan
 * @Date 2020/4/24 11:50
 * @Description
 **/
@Slf4j
@Service
@Transactional(noRollbackFor = NoRollbackException.class)
public class SystemNoticeService {

    @Autowired
    private SystemNoticeRepository systemNoticeRepository;

    @Autowired
    private SystemLogRepository systemLogRepository;

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailUtils emailUtils;

    public SystemNotice getSystemNotice() {
        return systemNoticeRepository.findTopBy();
    }

    public void deleteById(Integer id) {
        systemNoticeRepository.deleteById(id);
    }

    public void update(SystemNotice notice) {
        systemNoticeRepository.save(notice);
    }

    public void delete() {
        SystemNotice notice = getSystemNotice();
        if (notice != null) {
            systemNoticeRepository.deleteById(notice.getId());
            SecurityUser currentUser = ContextUtils.getCurrentUser();
            systemLogRepository.save(new SystemLog().setCreateTime(new Date()).setIp(IpUtils.getIp()).setContent(String.format(LogConstants.NOTICE_DELETE_MSG, currentUser.getUsername(), notice.getTitle(), notice.getContent())));
        }
    }

    public SystemNotice saveNotice(SystemNotice notice) {
        systemNoticeRepository.deleteAll();
        notice.setCreateTime(new Date());
        SecurityUser currentUser = ContextUtils.getCurrentUser();
        if (Objects.equals(notice.getIsShow(), 1) && Objects.equals(notice.getIsNotice(), 1)) {
            Lists.partition(userService.findUserEmails(), Optional.ofNullable(appConfig.getMailBatchSend()).orElse(10)).forEach(list -> {
                String emails = String.join(PublicConstants.DEFAULT_DELIMITER, String.join(PublicConstants.DEFAULT_DELIMITER, list));
                emailUtils.sendHtmlMail(emails, EmailConstants.SYSTEM_NOTICE_TITLE, notice.getContent());
                systemLogRepository.save(new SystemLog().setCreateTime(new Date()).setIp(IpUtils.getIp()).setContent(String.format(LogConstants.NOTICE_SEND_MSG, currentUser.getUsername(), emails, notice.getContent())));
            });
        }
        systemLogRepository.save(new SystemLog().setCreateTime(new Date()).setIp(IpUtils.getIp()).setContent(String.format(LogConstants.NOTICE_SAVE_MSG, currentUser.getUsername(), notice.getTitle(), notice.getContent())));
        return notice;
    }
}

