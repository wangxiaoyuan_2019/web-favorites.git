package com.wxy.favorites.service;

import com.wxy.favorites.constant.PublicConstants;
import com.wxy.favorites.core.NoRollbackException;
import com.wxy.favorites.dao.SearchTypeRepository;
import com.wxy.favorites.entity.SearchType;
import com.wxy.favorites.core.PageInfo;
import com.wxy.favorites.security.ContextUtils;
import com.wxy.favorites.security.SecurityUser;
import com.wxy.favorites.util.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.URL;
import java.util.List;

/**
 * @Author wangxiaoyuan
 * @Date 2020/4/24 11:50
 * @Description
 **/
@Slf4j
@Service
@Transactional(noRollbackFor = NoRollbackException.class)
public class SearchTypeService {

    @Autowired
    private SearchTypeRepository searchTypeRepository;

    public List<SearchType> findByUserId(Integer userId) {
        return searchTypeRepository.findByUserId(userId);
    }

    public PageInfo<SearchType> findPageByUserId(Integer userId, Integer pageNum, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize);
        Page<SearchType> page = searchTypeRepository.findPageByUserId(userId, pageable);
        return new PageInfo<>(page.getContent(), page.getTotalPages(), page.getTotalElements());
    }

    public SearchType save(SearchType searchType) {
        return searchTypeRepository.save(searchType);
    }

    public SearchType findById(Integer id) {
        return searchTypeRepository.findById(id).orElse(null);
    }

    public void deleteById(Integer id) {
        searchTypeRepository.deleteById(id);
    }

    public SearchType update(SearchType searchType) {
        return searchTypeRepository.save(searchType);
    }

    public void saveSearch(SearchType searchType) {
        String iconUrl;
        try {
            URL url = new URL(searchType.getUrl());
            iconUrl = url.getProtocol() + "://" + url.getHost() + (url.getPort() > 0 ? ":" + url.getPort() : "") + "/favicon.ico";
        } catch (Exception e) {
            iconUrl = PublicConstants.FAVORITES_ICON_DEFAULT;
        }
        if (searchType.getId() == null) {
            SecurityUser user = ContextUtils.getCurrentUser();
            searchType.setIcon(iconUrl);
            searchType.setUserId(user.getId());
            searchTypeRepository.save(searchType);
        } else {
            SearchType searchType1 = searchTypeRepository.findById(searchType.getId()).orElse(null);
            AssertUtils.notNull(searchType1, "搜索引擎不存在");
            searchType1.setName(searchType.getName());
            searchType1.setUrl(searchType.getUrl());
            searchType1.setIcon(iconUrl);
            searchTypeRepository.save(searchType1);
        }
    }
}

