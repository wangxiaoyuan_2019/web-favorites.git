package com.wxy.favorites.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.symmetric.AES;
import com.wxy.favorites.core.ApiResponse;
import com.wxy.favorites.core.NoRollbackException;
import com.wxy.favorites.dao.FavoritesRepository;
import com.wxy.favorites.dao.PasswordRepository;
import com.wxy.favorites.entity.Favorites;
import com.wxy.favorites.entity.Password;
import com.wxy.favorites.security.ContextUtils;
import com.wxy.favorites.util.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @Author wangxiaoyuan
 * @Date 2020/4/24 11:50
 * @Description
 **/
@Slf4j
@Service
@Transactional(noRollbackFor = NoRollbackException.class)
public class PasswordService {

    @Autowired
    private PasswordRepository passwordRepository;

    @Autowired
    private FavoritesRepository favoritesRepository;

    @Autowired
    private AES aes;

    public Password findByFavoritesId(Integer favoritesId) {
        Password password = passwordRepository.findByFavoritesId(favoritesId);
        if (password != null) {
            Password password1 = new Password().setId(password.getId()).setAccount(password.getAccount()).setPassword(password.getPassword()).setFavoritesId(password.getFavoritesId());
            Optional.ofNullable(password1.getAccount()).ifPresent(a -> password1.setAccount(aes.decryptStr(a)));
            Optional.ofNullable(password1.getPassword()).ifPresent(p -> password1.setPassword(aes.decryptStr(p)));
            return password1;
        } else {
            return null;
        }
    }

    public Password save(Password password) {
        Optional.ofNullable(password.getAccount()).ifPresent(a -> password.setAccount(aes.encryptBase64(a)));
        Optional.ofNullable(password.getPassword()).ifPresent(p -> password.setPassword(aes.encryptBase64(p)));
        return passwordRepository.save(password);
    }

    public Password update(Password password) {
        Optional.ofNullable(password.getAccount()).ifPresent(a -> password.setAccount(aes.encryptBase64(a)));
        Optional.ofNullable(password.getPassword()).ifPresent(p -> password.setPassword(aes.encryptBase64(p)));
        return passwordRepository.save(password);
    }

    public void deleteById(Integer id) {
        passwordRepository.deleteById(id);
    }

    public Password findById(Integer id) {
        return passwordRepository.findById(id).orElse(null);
    }

    public Integer savePwd(Password password) {
        Favorites favorites = favoritesRepository.findById(password.getFavoritesId()).orElse(null);
        AssertUtils.notNull(favorites, "收藏不存在");
        AssertUtils.isTrue(StrUtil.isNotBlank(password.getAccount()) || StrUtil.isNotBlank(password.getPassword()), "账户或密码不能为空");
        if (password.getId() == null) {
            password.setUserId(ContextUtils.getCurrentUser().getId());
            passwordRepository.save(password);
        } else {
            Password password1 = passwordRepository.findById(password.getId()).orElse(null);
            AssertUtils.notNull(password1, "密码不存在");
            password1.setAccount(password.getAccount());
            password1.setPassword(password.getPassword());
            passwordRepository.save(password1);
        }
        return password.getId();
    }
}
