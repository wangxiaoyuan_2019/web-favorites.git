package com.wxy.favorites.job;

import com.wxy.favorites.entity.WebContent;
import com.wxy.favorites.service.StatisticService;
import com.wxy.favorites.service.WebContentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Slf4j
public class WebContentJob {

    @Autowired
    private StatisticService statisticService;

    @Autowired
    private WebContentService webContentService;

    /**
     * 内容统计
     */
    @Scheduled(cron = "${cron.web-content-job}")
    @Async
    public void run() {
        try {
            log.info("内容统计任务开始执行...");
            WebContent webContent = statisticService.contentData();
            webContent.setCreateTime(new Date());
            webContentService.save(webContent);
        } catch (Exception e) {
            log.error("内容统计任务执行失败", e);
        }
    }
}
