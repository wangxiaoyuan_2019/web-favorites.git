package com.wxy.favorites.job;

import cn.hutool.core.date.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileFilter;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

/***
 * <p>
 * Description: 描述
 * </p>
 * @author wangxiaoyuan
 * 2021年10月09日
 */
@Component
@Slf4j
public class BackupJob {

    @Autowired
    private DataSource dataSource;

    @Value("${backup.file-path}")
    private String filePath;

    @Value("${backup.del-cycle}")
    private Integer delCycle;

    @Scheduled(cron = "${cron.backup-job}")
    @Async
    public void run() {
        try {
            log.info("数据库备份任务开始执行...");
            Connection conn = dataSource.getConnection();
            Statement stat = conn.createStatement();
            stat.execute(String.format("BACKUP TO '%s'", filePath + "/h2_" + DateUtil.today() + ".zip"));
            stat.close();
            conn.close();
            // 保留最近七天
            File[] files = new File(filePath).listFiles(file -> file.lastModified() < DateUtil.offsetDay(new Date(), delCycle).getTime());
            Optional.ofNullable(files).ifPresent(list -> Arrays.stream(list).forEach(File::deleteOnExit));
        } catch (Exception e) {
            log.error("数据库备份任务执行失败", e);
        }
    }
}
